package com.leaf.rssreader

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.windowsizeclass.ExperimentalMaterial3WindowSizeClassApi
import androidx.compose.material3.windowsizeclass.calculateWindowSizeClass
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.unit.Density
import coil.Coil
import coil.ImageLoader
import coil.decode.ImageDecoderDecoder
import coil.disk.DiskCache
import com.leaf.restorelibrary.InitAndRestoreService
import com.leaf.rssreader.pojo.Setting
import com.leaf.rssreader.repository.ChannelRepository
import com.leaf.rssreader.repository.SettingRepository
import com.leaf.rssreader.repository.TagRepository
import com.leaf.rssreader.ui.MainCompose
import com.leaf.rssreader.ui.theme.RssReaderTheme

class MainActivity : ComponentActivity() {

    companion object {
        private var initedFlag = false
    }

    @OptIn(ExperimentalMaterial3WindowSizeClassApi::class)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        init()

        setContent {
            RssReaderTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    val setting by Util.generateLifecycleAwareFlow(
                        flow = SettingRepository.settingStateFlow
                    ).collectAsState(initial = Setting())

                    CompositionLocalProvider(
                        LocalDensity provides Density(
                            density = LocalDensity.current.density,
                            fontScale = LocalDensity.current.fontScale + setting.getFontScaleExtra()
                        )
                    ) {
                        val widthSizeClass = calculateWindowSizeClass(this).widthSizeClass
                        MainCompose(widthSizeClass)
                    }
                }
            }
        }
    }

    override fun onPause() {
        ChannelRepository.onPause()
        super.onPause()
    }

    private fun init() {
        if (initedFlag) return
        initedFlag = true

        InitAndRestoreService.init(
            filesDir.toPath(),
            cacheDir.toPath(),
            listOf(
                Log,
                SettingRepository,
                ChannelRepository,
                TagRepository, // stay behind ChannelRepository
            )
        )

        // 使用 ImageLoaderFactory 的话需要在 Application class 中使用
        Coil.setImageLoader(
            ImageLoader.Builder(this)
                //.diskCachePolicy(CachePolicy.DISABLED) // 禁止掉可能会导致点击图片全屏时重新请求
                .diskCache {
                    DiskCache.Builder()
                        .directory(this.cacheDir.resolve("image_cache"))
                        .maxSizeBytes(25L * 1024 * 1024)
                        .build()
                }
                .okHttpClient {
                    DownloadService.okHttpClient.newBuilder()
                        .retryOnConnectionFailure(false)
                        .build()
                }
                .components {
                    add(ImageDecoderDecoder.Factory())
                }
                .placeholder(R.drawable.ic_baseline_image_24)
                .error(R.drawable.ic_baseline_broken_image_24)
                .build()
        )

        Thread.setDefaultUncaughtExceptionHandler { _, e ->
            Log.e(e.stackTraceToString())
            android.os.Process.killProcess(android.os.Process.myPid())
        }
    }

}
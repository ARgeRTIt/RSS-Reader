package com.leaf.rssreader.viewmodel

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.compose.runtime.snapshots.SnapshotStateList
import androidx.lifecycle.ViewModel
import com.leaf.rssreader.repository.TagRepository

class EditTagViewModel(private val originalName: String?): ViewModel() {

    var name by mutableStateOf(originalName ?: "")
    var channelSelectedList: SnapshotStateList<Int> = if (null == originalName) {
        mutableStateListOf()
    } else {
        val arr = TagRepository.getTagByName(originalName).channelIds.toTypedArray()
        mutableStateListOf(*arr)
    }
    var nameState by mutableStateOf(NameState.NONE)

    fun select(channelId: Int) {
        for (i in channelSelectedList.indices) {
            val tempChannelId = channelSelectedList[i]
            if (channelId == tempChannelId) {
                channelSelectedList.removeAt(i)
                return
            }
            if (channelId < tempChannelId) {
                channelSelectedList.add(i, channelId)
                return
            }
        }
        channelSelectedList.add(channelId)
    }

    fun save(): Boolean {
        if (name.isBlank()) {
            nameState = NameState.BLANK
            return false
        }

        if (null != originalName) {
            return TagRepository.modifyTag(originalName, name, channelSelectedList)
        }

        if (TagRepository.addTag(name, channelSelectedList)) return true
        nameState = NameState.DUPLICATE
        return false
    }

    enum class NameState {
        NONE,
        BLANK,
        DUPLICATE
    }

}
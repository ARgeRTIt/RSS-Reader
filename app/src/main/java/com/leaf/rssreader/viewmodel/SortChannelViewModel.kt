package com.leaf.rssreader.viewmodel

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import com.leaf.rssreader.repository.ChannelRepository

class SortChannelViewModel: ViewModel() {

    var channelIdselected by mutableStateOf(ChannelRepository.getInvalidChannelId())

    fun cancelSelect() {
        channelIdselected = ChannelRepository.getInvalidChannelId()
    }

    fun changePosition(destChannelId: Int, isFront: Boolean) {
        if (channelIdselected == destChannelId) return
        ChannelRepository.changeChannelPosition(
            channelIdselected,
            destChannelId,
            if (isFront) 0 else 1
        )
        cancelSelect()
    }

}
package com.leaf.rssreader.viewmodel

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.leaf.rssreader.DownloadService
import com.leaf.rssreader.HtmlParser
import com.leaf.rssreader.Util
import com.leaf.rssreader.pojo.Detail
import com.leaf.rssreader.repository.ChannelRepository
import com.leaf.rssreader.repository.DetailRepository
import kotlinx.coroutines.launch
import net.dankito.readability4j.Readability4J
import org.jsoup.Jsoup
import java.util.*

class DetailViewModel(tempId: String): ViewModel() {

    private val id = String(Base64.getUrlDecoder().decode(tempId))
    private var downloadingEntryUrl: String? = null

    val descriptionDetail: Detail
    val entry = ChannelRepository.getEntryByLink(id)
    val pubDateStr = Util.timestampToString(entry.pubDate)
    var state by mutableStateOf(State.NORMAL)
    var detail by mutableStateOf<Detail?>(null)

    init {
        descriptionDetail = HtmlParser.extractValidElements(
            Jsoup.parseBodyFragment(entry.description).body().childNodes()
        )

        viewModelScope.launch {
            DownloadService.progress.collect {
                println("------- collect, ${it.first}, ${it.second}")
                if (it.first != downloadingEntryUrl) return@collect

                when (it.second) {
                    DownloadService.State.SUCCESS -> {
                        downloadingEntryUrl = null
                        state = State.FULL
                    }
                    DownloadService.State.FAILURE -> {
                        downloadingEntryUrl = null
                        state = State.FULL_PULLING_FAIL
                    }
                    else -> {}
                }
                return@collect
            }
        }
    }

    fun pullFullHTML() {
        if (null == detail) detail = DetailRepository.getDetailByLink(entry.link)
        if (null != detail) {
            state = State.FULL
            return
        }

        doPullFullHTML()
    }

    enum class State {
        NORMAL,
        FULL,
        FULL_PULLING,
        FULL_PULLING_FAIL
    }

    // -------- private function BEGIN --------

    private fun doPullFullHTML() {
        val link = entry.link
        downloadingEntryUrl = link
        state = State.FULL_PULLING

        DownloadService.download(
            url = link
        ) { inputStream ->
            val responseText = inputStream.bufferedReader().use { it.readText() }

            val readability4J = Readability4J(link, responseText)
            val article = readability4J.parse()
            val processedText = article.content ?: ""

            val doc = Jsoup.parseBodyFragment(processedText)
            val elements = doc.select("[itemprop]")
            if (elements.isNotEmpty()) {
                for (element in elements) {
                    element.remove()
                }
            }

//            println(doc.outerHtml())
//            processedText = Cleaner(Safelist.relaxed()).clean(doc).html()

            val tempDetail = HtmlParser.extractValidElements(
                doc.body().firstElementChild()!!.childNodes()
            )
            DetailRepository.cacheDetail(link, tempDetail)
            detail = tempDetail
        }
    }

    // -------- private function END --------

}
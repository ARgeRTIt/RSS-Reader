package com.leaf.rssreader.viewmodel

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.compose.runtime.snapshots.SnapshotStateList
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.leaf.rssreader.DownloadService
import com.leaf.rssreader.pojo.FullScreenDTO
import kotlinx.coroutines.launch

class MainViewModel: ViewModel() {

    val downloadingUrls = SnapshotStateList<String>()
    var fullScreenDTO by mutableStateOf<FullScreenDTO?>(null)

    init {
        viewModelScope.launch {
            DownloadService.progress.collect {
                val url = it.first
                if (it.second == DownloadService.State.DOWNLOADING) {
                    downloadingUrls.add(url)
                } else {
                    downloadingUrls.remove(url)
                }
            }
        }
    }

}
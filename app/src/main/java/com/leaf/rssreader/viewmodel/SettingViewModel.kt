package com.leaf.rssreader.viewmodel

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel

class SettingViewModel: ViewModel() {

    var dialogId by mutableStateOf(DialogId.NONE)

    enum class DialogId {
        NONE,
        READ_ENTRIES_CACHED_DAYS,
    }

    fun clearDialogId() {
        dialogId = DialogId.NONE
    }

}
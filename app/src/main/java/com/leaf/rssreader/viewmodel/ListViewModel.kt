package com.leaf.rssreader.viewmodel

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.leaf.rssreader.DownloadService
import com.leaf.rssreader.repository.ChannelRepository
import kotlinx.coroutines.launch

class ListViewModel(private val channelId: Int): ViewModel() {

    private var downloadingUrl: String? = null
    // 不需要 StateFlow: 修改完频道后，会跳转到标签界面
    val description = ChannelRepository.getChannelDescription(channelId)
    var entryIdWithDivider: String? = null

    var isShowUnread by mutableStateOf(true)
    var refreshState by mutableStateOf(RefreshState.NORMAL)

    init {
        viewModelScope.launch {
            DownloadService.progress.collect {
                if (it.first == downloadingUrl) {
                    when (it.second) {
                        DownloadService.State.SUCCESS -> {
                            downloadingUrl = null
                            refreshState = RefreshState.NORMAL
                        }
                        DownloadService.State.FAILURE -> {
                            downloadingUrl = null
                            refreshState = RefreshState.FAILURE
                        }
                        else -> {}
                    }
                }
            }
        }
    }

    fun refresh() {
        if (RefreshState.REFRESHING == refreshState) return

        downloadingUrl = ChannelRepository.getChannelUrl(channelId)
        refreshState = RefreshState.REFRESHING

        DownloadService.downloadFeed(
            channelId = channelId,
            url = downloadingUrl!!
        )
    }

    fun switchShowState() {
        if (RefreshState.NORMAL != refreshState) refreshState = RefreshState.NORMAL

        entryIdWithDivider = null
        isShowUnread = !isShowUnread
    }

    enum class RefreshState {
        NORMAL,
        REFRESHING,
        FAILURE
    }

}
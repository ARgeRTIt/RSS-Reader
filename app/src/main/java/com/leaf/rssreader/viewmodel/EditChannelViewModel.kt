package com.leaf.rssreader.viewmodel

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.leaf.rssreader.DownloadService
import com.leaf.rssreader.FeedParser
import com.leaf.rssreader.pojo.Channel
import com.leaf.rssreader.pojo.Entry
import com.leaf.rssreader.repository.ChannelRepository
import com.leaf.rssreader.repository.TagRepository
import kotlinx.coroutines.launch

private const val URL_PREFIX = "https://"

class EditChannelViewModel(private val id: Int?): ViewModel() {

    var description by mutableStateOf(if (null != id)
        ChannelRepository.getChannelDescription(id)
    else
        ""
    )
    var url by mutableStateOf(if (null != id)
        ChannelRepository.getChannelUrl(id)
    else
        URL_PREFIX
    )
    var descriptionState by mutableStateOf(DescriptionState.NONE)
    var urlState by mutableStateOf(UrlState.NONE)
    val tagSelectedList = if (null != id)
        mutableStateListOf(*TagRepository.getTagNamesByChannelId(id))
    else
        mutableStateListOf()
    var saveCallback by mutableStateOf<(() -> Unit)?>(null)

    init {
        viewModelScope.launch {
            DownloadService.progress.collect {
                // other fragment can download the same url
                if (null == saveCallback) return@collect
                if (it.first != url) return@collect
                when (it.second) {
                    DownloadService.State.SUCCESS -> {
                        saveCallback!!()
                        saveCallback = null
                    }
                    DownloadService.State.FAILURE -> {
                        urlState = UrlState.CANNOT_ACCESS
                        saveCallback = null
                    }
                    else -> {}
                }
            }
        }
    }

    fun select(tagName: String) {
        val index = tagSelectedList.indexOf(tagName)
        if (index < 0) tagSelectedList.add(tagName)
        else tagSelectedList.removeAt(index)
    }

    fun save(successCallback: () -> Unit) {
        if (null != saveCallback) return

        if (description.isBlank()) {
            descriptionState = DescriptionState.BLANK
            return
        }
        if (url.isBlank()) {
            urlState = UrlState.BLANK
            return
        }
        if ((!url.startsWith(URL_PREFIX)) || url.length == URL_PREFIX.length) {
            urlState = UrlState.NOT_HTTPS
            return
        }
        if (description.contains('\n')) {
            descriptionState = DescriptionState.CONTAIN_LINE_BREAK
            return
        }
        if (url.contains('\n')) {
            urlState = UrlState.CONTAIN_LINE_BREAK
            return
        }

        if (null != id && ChannelRepository.getChannelUrl(id) == url) {
            doSave(id, url, description.trim(), null, tagSelectedList)
            successCallback()
            return
        }

        saveCallback = successCallback
        DownloadService.download(url) { inputStream ->
            val tempEntries = FeedParser.parser(inputStream, 0)
            doSave(
                id ?: ChannelRepository.getInvalidChannelId(),
                url,
                description.trim(),
                tempEntries,
                tagSelectedList
            )
        }
    }

    enum class DescriptionState(val str: String) {
        NONE("none"),
        BLANK("blank"),
        CONTAIN_LINE_BREAK("contains line break")
    }

    enum class UrlState(val str: String) {
        NONE("none"),
        BLANK("blank"),
        NOT_HTTPS("not https"),
        CONTAIN_LINE_BREAK("contains line break"),
        CANNOT_ACCESS("can not access"),
    }

    // -------- private function begin --------

    private fun doSave(
        id: Int,
        url: String,
        description: String,
        entries: MutableList<Entry>?,
        tagList: List<String>
    ) {
        val realId = ChannelRepository.saveChannel(Channel(id, url, description), entries)
        TagRepository.modifyChannelTags(realId, tagList)
    }

    // -------- private function end --------

}
package com.leaf.rssreader

import android.util.Log
import android.util.Xml
import com.leaf.rssreader.pojo.Entry
import org.xmlpull.v1.XmlPullParser
import java.io.InputStream
import java.time.ZoneId
import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter
import java.time.temporal.ChronoField
import java.time.temporal.TemporalQueries

object FeedParser {

    private const val TAG = "FeedParser"
    private val ns: String? = null

    private val RSS_VALID_DATE_TIME_FORMATTER = listOf(
        DateTimeFormatter.RFC_1123_DATE_TIME,
        DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"),
        DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss Z")
    )
    private val ATOM_VALID_DATE_TIME_FORMATTER = listOf(
        DateTimeFormatter.ISO_OFFSET_DATE_TIME
    )

    fun parser(inputStream: InputStream, timeAfter: Long): MutableList<Entry> {
        val parser = Xml.newPullParser()
        parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false)
        parser.setInput(inputStream, null)
        parser.nextTag()
        return readFeed(parser, timeAfter)
    }

    // -------- private function BEGIN --------

    private fun readFeed(parser: XmlPullParser, timeAfter: Long): MutableList<Entry> {
        return when (parser.name) {
            FirstLevelTag.RSS -> readRss(parser, timeAfter)
            FirstLevelTag.ATOM -> readAtom(parser, timeAfter)
            else -> {
                check(false)
                return mutableListOf()
            }
        }
    }

    private fun readRss(parser: XmlPullParser, timeAfter: Long): MutableList<Entry> {
        val entries = mutableListOf<Entry>()
        val idMap = HashMap<String, Boolean>()
        var alternativeDate: Long? = null
        val readEntryFunc: () -> Entry? = {
            readRssEntry(parser, alternativeDate)
        }
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.eventType != XmlPullParser.START_TAG) {
                continue
            }

            if ("channel" != parser.name) {
                skip(parser)
                continue
            }

            while (parser.next() != XmlPullParser.END_TAG) {
                if (parser.eventType != XmlPullParser.START_TAG) {
                    continue
                }
                when (val tag = parser.name) {
                    "lastBuildDate" -> {
                        alternativeDate = readPubDate(
                            parser,
                            tag,
                            RSS_VALID_DATE_TIME_FORMATTER
                        )
                    }
                    "item" -> {
                        readEntry(
                            readEntryFunc = readEntryFunc,
                            timeAfter = timeAfter,
                            idMap = idMap,
                            entries = entries
                        )
                    }
                    else -> skip(parser)
                }
            }
            break // 只解析一个 channel
        }
        return entries
    }

    private fun readAtom(parser: XmlPullParser, timeAfter: Long): MutableList<Entry> {
        val entries = mutableListOf<Entry>()
        val idMap = HashMap<String, Boolean>()
        var alternativeDate: Long? = null
        val readEntryFunc: () -> Entry? = {
            readAtomEntry(parser, alternativeDate)
        }
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.eventType != XmlPullParser.START_TAG) {
                continue
            }

            when (val tag = parser.name) {
                "updated" -> {
                    alternativeDate = readPubDate(
                        parser,
                        tag,
                        ATOM_VALID_DATE_TIME_FORMATTER
                    )
                }
                "entry" -> {
                    readEntry(
                        readEntryFunc = readEntryFunc,
                        timeAfter = timeAfter,
                        idMap = idMap,
                        entries = entries
                    )
                }
                else -> skip(parser)
            }
        }
        return entries
    }

    private fun readEntry(
        readEntryFunc: () -> Entry?,
        timeAfter: Long,
        idMap: MutableMap<String, Boolean>,
        entries: MutableList<Entry>,
    ) {
        val entry = readEntryFunc() ?: return
        if (entry.pubDate <= timeAfter) return
        val id = entry.getUniqueId()
        if (idMap.containsKey(id)) return
        entries.add(entry)
        idMap[id] = true
    }

    private fun readRssEntry(
        parser: XmlPullParser,
        alternativeDate: Long?
    ): Entry? {
        var title: String? = null
        var description: String? = null
        var pubDate: Long? = null
        var link: String? = null
        var contentEncoded: String? = null
        var guid: String? = null

        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.eventType != XmlPullParser.START_TAG) {
                continue
            }
            when (val tag = parser.name) {
                RssTag.ENTRY_TITLE -> title = readText(parser, tag)
                RssTag.ENTRY_DESCRIPTION -> description = readText(parser, tag)
                RssTag.ENTRY_PUB_DATE -> pubDate = readPubDate(
                    parser,
                    tag,
                    RSS_VALID_DATE_TIME_FORMATTER
                ) ?: return null
                RssTag.ENTRY_LINK -> link = Util.processUrl(readText(parser, tag))
                RssTag.ENTRY_CONTENT_ENCODED -> contentEncoded = readText(parser, tag)
                RssTag.ENTRY_GUID -> guid = readText(parser, tag)
                else -> skip(parser)
            }
        }
        checkNotNull(title)
        check(null != pubDate || null != alternativeDate)
        checkNotNull(link)
        return Entry(
            title = title,
            description = contentEncoded ?: description ?: "",
            pubDate = pubDate ?: alternativeDate!!,
            link = link,
            primaryId = if (guid != link) guid else null
        )
    }

    private fun readAtomEntry(
        parser: XmlPullParser,
        alternativeDate: Long?
    ): Entry? {
        var title: String? = null
        var link: String? = null
        var pubDate: Long? = null
        var summary: String? = null
        var content: String? = null
        var id: String? = null

        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.eventType != XmlPullParser.START_TAG) {
                continue
            }
            when (val tag = parser.name) {
                AtomTag.ENTRY_TITLE -> title = readText(parser, tag)
                AtomTag.ENTRY_LINK -> {
                    link = Util.processUrl(parser.getAttributeValue(ns, "href"))
                    skip(parser)
                }
                AtomTag.ENTRY_UPDATED -> pubDate = readPubDate(
                    parser,
                    tag,
                    ATOM_VALID_DATE_TIME_FORMATTER
                ) ?: return null
                AtomTag.ENTRY_SUMMARY -> summary = readText(parser, tag)
                AtomTag.ENTRY_CONTENT -> content = readText(parser, tag)
                AtomTag.ENTRY_ID -> id = readText(parser, tag)
                else -> skip(parser)
            }
        }
        checkNotNull(title)
        checkNotNull(link)
        check(null != pubDate || null != alternativeDate)
        checkNotNull(id)
        return Entry(
            title = title,
            description = content ?: summary ?: "",
            pubDate = pubDate ?: alternativeDate!!,
            link = link,
            primaryId = id
        )
    }

    private fun readText(parser: XmlPullParser, tag: String): String {
        val text = doReadText(parser)
        parser.require(XmlPullParser.END_TAG, ns, tag)
        return text
    }

    private fun readPubDate(
        parser: XmlPullParser,
        tag: String,
        dateTimeFormatters: List<DateTimeFormatter>
    ): Long? {
        val text = doReadText(parser).replace(Regex("""\s+"""), " ")
        parser.require(XmlPullParser.END_TAG, ns, tag)
        for (dateTimeFormatter in dateTimeFormatters) {
            try {
                return parseDateTime(dateTimeFormatter, text)
            } catch (_: Exception) {}
        }
        Log.e(TAG, "readPubDate, no match pattern. tag=$tag, text='$text'")
        return null
    }

    private fun parseDateTime(dateTimeFormatter: DateTimeFormatter, text: String): Long {
        val temporalAccessor = dateTimeFormatter.parse(text)
        return ZonedDateTime.of(
            temporalAccessor.get(ChronoField.YEAR),
            temporalAccessor.get(ChronoField.MONTH_OF_YEAR),
            temporalAccessor.get(ChronoField.DAY_OF_MONTH),
            temporalAccessor.get(ChronoField.HOUR_OF_DAY),
            temporalAccessor.get(ChronoField.MINUTE_OF_HOUR),
            temporalAccessor.get(ChronoField.SECOND_OF_MINUTE),
            0,
            temporalAccessor.query(TemporalQueries.zone()) ?: ZoneId.systemDefault()
        ).toEpochSecond()
    }

    private fun doReadText(parser: XmlPullParser): String {
        var result = ""
        if (parser.next() == XmlPullParser.TEXT) {
            result = parser.text
            parser.nextTag()
        }
        return Util.processText(result)
    }

    private fun skip(parser: XmlPullParser) {
        if (parser.eventType != XmlPullParser.START_TAG) {
            throw IllegalStateException()
        }
        var depth = 1
        while (depth != 0) {
            when (parser.next()) {
                XmlPullParser.END_TAG -> depth--
                XmlPullParser.START_TAG -> depth++
            }
        }
    }

    private object FirstLevelTag {
        const val RSS = "rss"
        const val ATOM = "feed"
    }

    private object RssTag {
        const val ENTRY_TITLE = "title"
        const val ENTRY_DESCRIPTION = "description"
        const val ENTRY_PUB_DATE = "pubDate"
        const val ENTRY_LINK = "link"
        const val ENTRY_CONTENT_ENCODED = "content:encoded"
        const val ENTRY_GUID = "guid"
    }

    private object AtomTag {
        const val ENTRY_TITLE = "title"
        const val ENTRY_LINK = "link"
        const val ENTRY_UPDATED = "updated"
        const val ENTRY_SUMMARY = "summary"
        const val ENTRY_CONTENT = "content"
        const val ENTRY_ID = "id"
    }

    // -------- private function END --------

}
package com.leaf.rssreader

import com.leaf.restorelibrary.RestoreComponentAbstract
import java.io.BufferedWriter
import java.nio.file.Path
import java.nio.file.StandardOpenOption
import kotlin.io.path.bufferedReader
import kotlin.io.path.notExists
import kotlin.io.path.outputStream

object Log: RestoreComponentAbstract() {

    private const val CACHE_NAME = "log.txt"
    private lateinit var filePath: Path
    private lateinit var bufferedWriter: BufferedWriter

    override fun load() {
        filePath = cachePath.resolve(CACHE_NAME)
        bufferedWriter = filePath
            .outputStream(StandardOpenOption.CREATE, StandardOpenOption.APPEND)
            .bufferedWriter()
        bufferedWriter.appendLine("\n--------------- init ---------------\n")
    }

    fun e(str: String) {
        appendAndFlush("error", str)
        println(str)
    }

    fun getAllLog(): String {
        if (filePath.notExists()) return ""
        return filePath.bufferedReader().use { it.readText() }
    }

    private fun appendAndFlush(type: String, str: String) {
        if (filePath.notExists()) restore()

        bufferedWriter.appendLine(
            "[${Util.timestampToString(Util.getCurrentTimestamp())}] [$type] $str"
        )
        bufferedWriter.flush()
    }

}
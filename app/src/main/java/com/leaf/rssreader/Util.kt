package com.leaf.rssreader

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.platform.LocalLifecycleOwner
import androidx.lifecycle.flowWithLifecycle
import kotlinx.coroutines.flow.Flow
import java.time.Instant
import java.time.LocalDateTime
import java.time.ZoneId
import java.time.format.DateTimeFormatter

object Util {

    fun getCurrentTimestamp(): Long {
        return Instant.now().epochSecond
    }

    fun getCurrentLocalDateTime(): LocalDateTime {
        return LocalDateTime.now().withNano(0)
    }

    fun timestampToString(second: Long): String {
        return timestampToLocalDateTime(second)
            .format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm"))
    }

    fun timestampToLocalDateTime(second: Long): LocalDateTime {
        return LocalDateTime.ofInstant(
            Instant.ofEpochSecond(second),
            ZoneId.systemDefault()
        )
    }

    fun processUrl(url: String): String {
        return if (url.startsWith("http:"))
            "https" + url.substring(4)
        else
            url
//        return httpsUrl.substringBefore('?')
    }

    fun processText(text: String): String {
        return text.trim {
            it.isWhitespace() || it == '　'
        }
    }

    @Composable
    fun <T> generateLifecycleAwareFlow(flow: Flow<T>): Flow<T> {
        val lifecycleOwner = LocalLifecycleOwner.current
        return remember(flow, lifecycleOwner) {
            flow.flowWithLifecycle(lifecycleOwner.lifecycle)
        }
    }

    fun isWIFI(context: Context): Boolean {
        val connectivityManager = context.getSystemService(ConnectivityManager::class.java)
        val currentNetwork = connectivityManager.activeNetwork
        val caps = connectivityManager.getNetworkCapabilities(currentNetwork)
        return caps?.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) ?: false
    }

}
package com.leaf.rssreader.repository

import com.leaf.restorelibrary.RestoreComponentAbstract
import com.leaf.rssreader.Util
import com.leaf.rssreader.pojo.Channel
import com.leaf.rssreader.pojo.Entry
import com.leaf.rssreader.pojo.EntryCache
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import org.json.JSONArray
import org.json.JSONObject
import java.nio.file.Path
import kotlin.io.path.*

object ChannelRepository: RestoreComponentAbstract() {

    private const val CHANNEL_PERSISTENT_NAME = "channels.json"
    private const val ENTRIES_CACHE_NAME_FORMAT = "%d.json"

    private lateinit var channelMap: MutableMap<Int, Channel>
    private lateinit var channelIdSequence: MutableList<Int>
    private val idToEntryMap: MutableMap<String, Entry> = HashMap()
    private val channelListMutableStateFlow =
        MutableStateFlow<List<Channel>>(mutableListOf())
    private val channelIdToEntryCache = HashMap<Int, EntryCache>()
    private val channelIdReadSavedMap: MutableMap<Int, Boolean> = HashMap()

    var channelListStateFlow: StateFlow<List<Channel>> = channelListMutableStateFlow

    override fun init(persistentPath: Path, cachePath: Path) {
        this.persistentPath = persistentPath
        this.cachePath = cachePath.resolve("entry_cache").createDirectories()
        load()
    }

    override fun restore() {
        idToEntryMap.clear()
        channelIdToEntryCache.clear()
        channelIdReadSavedMap.clear()
        this.cachePath.createDirectories()
        super.restore()
    }

    override fun getPersistentFileNames(): List<String> {
        return listOf(CHANNEL_PERSISTENT_NAME)
    }

    fun onPause() {
        if (channelIdReadSavedMap.isEmpty()) return
        channelIdReadSavedMap.keys.toTypedArray().forEach {
            saveEntryCache(it)
        }
    }

    fun getEntryCacheLastRefreshTimestamp(channelId: Int): Long {
        return channelIdToEntryCache[channelId]!!.lastRefreshTimestamp
    }

    fun getUnreadEntriesByChannelId(channelId: Int): StateFlow<List<Entry>> {
        val cache = channelIdToEntryCache[channelId]!!
        return cache.unreadEntries
    }

    fun getReadEntriesByChannelId(channelId: Int): StateFlow<List<Entry>> {
        val cache = channelIdToEntryCache[channelId]!!
        return cache.getReadEntries()
    }

    fun readEntry(channelId: Int, id: String) {
        if ((!idToEntryMap.containsKey(id))) return
        val entryCache = channelIdToEntryCache[channelId]!!
        val tempEntries = entryCache.unreadEntries.value
        var unreadIndex = -1
        var pubDate = 0L
        for (i in tempEntries.indices) {
            val tempEntry = tempEntries[i]
            if (id == tempEntry.getUniqueId()) {
                unreadIndex = i
                pubDate = tempEntry.pubDate
                break
            }
        }
        if (unreadIndex < 0) return

        val unreadEntries = tempEntries.toMutableList()
        val readEntries = channelIdToEntryCache[channelId]!!
            .getReadEntries()
            .value
            .toMutableList()
        var readIndex = -1
        for (i in readEntries.indices) {
            if (pubDate >= readEntries[i].pubDate) {
                readIndex = i
                break
            }
        }
        if (readIndex < 0) readIndex = readEntries.size
        readEntries.add(readIndex, unreadEntries.removeAt(unreadIndex).copy(
            readDate = Util.getCurrentTimestamp()
        ))

        channelIdReadSavedMap[channelId] = true
        entryCache.unreadEntries.value = unreadEntries
        entryCache.setReadEntries(readEntries)
    }

    fun readAllEntries(channelId: Int) {
        val entryCache = channelIdToEntryCache[channelId]!!
        val unreadEntries = entryCache.unreadEntries.value
        if (unreadEntries.isEmpty()) return

        val currentTimestamp = Util.getCurrentTimestamp()
        val readEntries = entryCache.getReadEntries().value.toMutableList()
        val iter = readEntries.listIterator()
        val addFunc = fun (entry: Entry) {
            iter.add(entry.copy(
                readDate = currentTimestamp
            ))
        }

        for (entry in unreadEntries) {
            var flag = true
            while (iter.hasNext()) {
                val tempEntry = readEntries[iter.nextIndex()]
                if (entry.pubDate < tempEntry.pubDate) {
                    iter.next()
                } else {
                    addFunc(entry)
                    flag = false
                    break
                }
            }
            if (flag) addFunc(entry)
        }
        entryCache.unreadEntries.value = emptyList()
        entryCache.setReadEntries(readEntries)
        saveEntryCache(channelId)
    }

    fun saveNewEntries(
        channelId: Int,
        entries: MutableList<Entry>,
        currentTimestamp: Long,
        flag: Boolean = true // 只有 saveChannel 中需要设置
    ) {
        if (flag) {
            entries.removeIf {
                idToEntryMap.containsKey(it.getUniqueId())
            }
        } else {
            removeEntryCache(channelId)
            loadEntryCache(channelId)
        }

        val entryCache = channelIdToEntryCache[channelId]!!
        entryCache.lastRefreshTimestamp = currentTimestamp
        if (entries.isEmpty()) return

        val unreadMutableStateFlow = entryCache.unreadEntries
        val tempList = unreadMutableStateFlow.value.toMutableList()
        for (entry in entries.asReversed()) {
            idToEntryMap[entry.getUniqueId()] = entry
            insertIntoEntryMutableList(tempList, entry)
        }

        unreadMutableStateFlow.value = tempList
        saveEntryCache(channelId)
    }

    fun getEntryByLink(id: String): Entry {
        return idToEntryMap[id]!!
    }

    fun isValidChannelId(id: Int): Boolean {
        return channelMap.containsKey(id)
    }

    fun getChannelDescription(id: Int): String {
        return channelMap[id]!!.description
    }

    fun getChannelUrl(id: Int): String {
        return channelMap[id]!!.url
    }

    fun saveChannel(tempChannel: Channel, entries: MutableList<Entry>?): Int {
        val tempId = tempChannel.id
        val url = tempChannel.url

        val channel = if (isChannelIdValid(tempId)) {
            if (tempChannel == channelMap[tempId]) return tempId
            tempChannel
        } else {
            for ((_, channel) in channelMap) {
                if (url == channel.url) return channel.id
            }
            tempChannel.copy(id = generateId())
        }

        val realId = channel.id
        channelMap[realId] = channel
        if (!isChannelIdValid(tempId)) {
            channelIdSequence.add(realId)
            loadEntryCache(realId)
        }

        doSaveChannel()
        if (null != entries) {
            saveNewEntries(realId, entries, Util.getCurrentTimestamp(), false)
        }

        return realId
    }

    fun removeChannel(channelId: Int) {
        channelIdSequence.remove(channelId)
        channelMap.remove(channelId) ?: return

        doSaveChannel()
        channelIdReadSavedMap.remove(channelId)

        removeEntryCache(channelId)
    }

    fun getInvalidChannelId(): Int {
        return -1
    }

    fun isChannelIdValid(channelId: Int): Boolean {
        return channelId > 0
    }

    fun changeChannelPosition(
        selectedChannelId: Int,
        destChannelId: Int,
        offset: Int
    ) {
        val selectedIndex = channelIdSequence.indexOf(selectedChannelId)
        if (selectedChannelId < 0 || !channelMap.containsKey(destChannelId)) return

        channelIdSequence.removeAt(selectedIndex)
        val destIndex = channelIdSequence.indexOf(destChannelId)
        channelIdSequence.add(destIndex + offset, selectedChannelId)

        doSaveChannel()
    }

    // -------- private function BEGIN --------

    override fun load() {
        channelMap = HashMap()
        channelIdSequence = mutableListOf()
        val path = persistentPath.resolve(CHANNEL_PERSISTENT_NAME)
        if (path.exists()) {
            val text = path.bufferedReader().use { it.readText() }
            val channelList = convertJsonStringToChannelList(text)
            for (channel in channelList) {
                channelMap[channel.id] = channel
                channelIdSequence.add(channel.id)
            }
        }

        updateChannelListMutableStateFlow()

        channelIdSequence.forEach {
            loadEntryCache(it)
        }
    }

    private fun updateChannelListMutableStateFlow() {
        val channelList = mutableListOf<Channel>()
        channelIdSequence.forEach {
            channelList.add(channelMap[it]!!)
        }
        channelListMutableStateFlow.value = channelList
    }

    private fun insertIntoEntryMutableList(entries: MutableList<Entry>, entry: Entry) {
        for (i in entries.indices) {
            if (entry.pubDate >= entries[i].pubDate) {
                entries.add(i, entry)
                return
            }
        }
        entries.add(entry)
    }

    private fun generateId(): Int {
        var id = 1
        while (id <= Int.MAX_VALUE) {
            if (!channelMap.containsKey(id)) {
                return id
            }
            ++id
        }
        check(false)
        return 0
    }

    private fun convertJsonStringToChannelList(str: String): List<Channel> {
        val list = mutableListOf<Channel>()
        val arr = JSONArray(str)
        for (i in 0 until arr.length()) {
            list.add(Channel.convertFromJson(arr.getJSONObject(i)))
        }
        return list
    }

    private fun convertChannelsToJsonString(): String {
        val arr = JSONArray()
        channelIdSequence.forEach {
            arr.put(channelMap[it]!!.convertToJson())
        }
        return arr.toString(4)
    }

    private fun doSaveChannel() {
        val text = convertChannelsToJsonString()
        val path = persistentPath.resolve(CHANNEL_PERSISTENT_NAME)
        path.bufferedWriter().use { it.write(text) }

        updateChannelListMutableStateFlow()
    }

    private fun loadEntryCache(channelId: Int) {
        val fileName = String.format(ENTRIES_CACHE_NAME_FORMAT, channelId)
        val path = cachePath.resolve(fileName)
        val entryCache = if (path.exists()) {
            val text = path.bufferedReader().use { it.readText() }
            EntryCache.convertFromJson(JSONObject(text))
        } else {
            EntryCache(
                0L,
                MutableStateFlow(emptyList()),
                MutableStateFlow(emptyList()),
            )
        }
        channelIdToEntryCache[channelId] = entryCache

        for (entry in entryCache.unreadEntries.value) {
            val id = entry.getUniqueId()
            check(!idToEntryMap.containsKey(id))
            idToEntryMap[id] = entry
        }
        for (entry in entryCache.getReadEntries().value) {
            val id = entry.getUniqueId()
            check(!idToEntryMap.containsKey(id))
            idToEntryMap[id] = entry
        }
    }

    private fun saveEntryCache(channelId: Int) {
        val entryCache = channelIdToEntryCache[channelId] ?: return
        val fileName = String.format(ENTRIES_CACHE_NAME_FORMAT, channelId)
        val path = cachePath.resolve(fileName)
        val text = entryCache.convertToJson().toString(4)
        path.bufferedWriter().use { it.write(text) }
        channelIdReadSavedMap.remove(channelId)
    }

    private fun removeEntryCache(channelId: Int) {
        val fileName = String.format(ENTRIES_CACHE_NAME_FORMAT, channelId)
        val path = cachePath.resolve(fileName)
        path.deleteIfExists()

        val entryCache = channelIdToEntryCache.remove(channelId) ?: return
        for (entry in entryCache.unreadEntries.value) {
            idToEntryMap.remove(entry.getUniqueId())
            DetailRepository.removeDetail(entry.link)
        }
        for (entry in entryCache.getReadEntries().value) {
            idToEntryMap.remove(entry.getUniqueId())
            DetailRepository.removeDetail(entry.link)
        }
    }

    // -------- private function END --------

}
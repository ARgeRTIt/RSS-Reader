package com.leaf.rssreader.repository

import android.content.Context
import com.leaf.restorelibrary.RestoreComponentAbstract
import com.leaf.rssreader.Util
import com.leaf.rssreader.pojo.Setting
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlin.io.path.bufferedReader
import kotlin.io.path.bufferedWriter
import kotlin.io.path.exists

object SettingRepository: RestoreComponentAbstract() {

    private const val PERSISTENT_NAME = "setting.json"
    private val settingMutableStateFlow: MutableStateFlow<Setting> = MutableStateFlow(
        Setting()
    )

    val settingStateFlow: StateFlow<Setting> = settingMutableStateFlow

    override fun getPersistentFileNames(): List<String> {
        return listOf(PERSISTENT_NAME)
    }

    fun canShowDescriptionImage(context: Context): Boolean {
        return settingMutableStateFlow.value.mobileNetworkDescriptionImageShow
                || Util.isWIFI(context)
    }

    fun canShowFullContentImage(context: Context): Boolean {
        return settingMutableStateFlow.value.mobileNetworkFullContentImageShow
                || Util.isWIFI(context)
    }

    fun setMobileNetworkDescriptionImageShowFlag(flag: Boolean) {
        if (settingMutableStateFlow.value.mobileNetworkDescriptionImageShow == flag) return
        settingMutableStateFlow.value = settingMutableStateFlow.value.copy(
            mobileNetworkDescriptionImageShow = flag
        )
        save()
    }

    fun setMobileNetworkFullContentImageShowFlag(flag: Boolean) {
        if (settingMutableStateFlow.value.mobileNetworkFullContentImageShow == flag) return
        settingMutableStateFlow.value = settingMutableStateFlow.value.copy(
            mobileNetworkFullContentImageShow = flag
        )
        save()
    }

    fun setReadEntriesCachedDays(days: Int) {
        if (settingMutableStateFlow.value.readEntriesCachedDays == days) return
        settingMutableStateFlow.value = settingMutableStateFlow.value.copy(
            readEntriesCachedDays = days
        )
        save()
    }

    fun setFontScaleExtraMultiplyBy10(extraMultiplyBy10: Int) {
        if (settingMutableStateFlow.value.fontScaleExtraMultiplyBy10 == extraMultiplyBy10) return
        settingMutableStateFlow.value = settingMutableStateFlow.value.copy(
            fontScaleExtraMultiplyBy10 = extraMultiplyBy10
        )
        save()
    }

    // -------- private function BEGIN --------

     override fun load() {
        val path = persistentPath.resolve(PERSISTENT_NAME)
        if (path.exists()) {
            val text = path.bufferedReader().use { it.readText() }
            settingMutableStateFlow.value = Setting.convertFromJsonString(text)
        }
    }

    private fun save() {
        val text = settingMutableStateFlow.value.convertToJsonString()
        val path = persistentPath.resolve(PERSISTENT_NAME)
        path.bufferedWriter().use { it.write(text) }
    }

    // -------- private function END --------

}
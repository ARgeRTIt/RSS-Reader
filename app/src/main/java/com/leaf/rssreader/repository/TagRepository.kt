package com.leaf.rssreader.repository

import com.leaf.restorelibrary.RestoreComponentAbstract
import com.leaf.rssreader.pojo.Tag
import org.json.JSONArray
import kotlin.io.path.bufferedReader
import kotlin.io.path.bufferedWriter
import kotlin.io.path.exists

object TagRepository: RestoreComponentAbstract() {

    private const val PERSISTENT_NAME = "tags.json"
    private lateinit var nameToTag: MutableMap<String, Tag>

    override fun getPersistentFileNames(): List<String> {
        return listOf(PERSISTENT_NAME)
    }

    fun getTagByName(name: String): Tag {
        return nameToTag[name]!!
    }

    fun getAllTag(): Map<String, Tag> {
        return nameToTag
    }

    fun getTagNamesByChannelId(channelId: Int): Array<String> {
        val ret = mutableListOf<String>()
        for ((name, tag) in nameToTag) {
            if (tag.channelIds.contains(channelId)) ret.add(name)
        }
        return ret.toTypedArray()
    }

    fun addTag(name: String, channelIds: MutableList<Int> = mutableListOf()): Boolean {
        if (nameToTag.containsKey(name)) return false
        nameToTag[name] = Tag(
            name = name,
            channelIds = channelIds
        )
        save()
        return true
    }

    fun modifyTag(
        oldName: String,
        newName: String,
        channelIds: MutableList<Int>
    ): Boolean {
        if (!nameToTag.containsKey(oldName)) return false
        val isDifferentName = oldName != newName
        if (isDifferentName && nameToTag.containsKey(newName)) return false

        if (isDifferentName) nameToTag.remove(oldName)
        nameToTag[newName] = Tag(
            name = newName,
            channelIds = channelIds
        )
        save()
        return true
    }

    fun removeTag(name: String) {
        if (!nameToTag.containsKey(name)) return
        nameToTag.remove(name)
        save()
    }

    fun modifyChannelTags(channelId: Int, tagNames: List<String>) {
        var modifiedFlag = doRemoveChannelAllTag(channelId)
        if (tagNames.isEmpty()) {
            if (modifiedFlag) save()
            return
        }

        for (name in tagNames) {
            val tag = nameToTag[name] ?: continue
            if (tag.channelIds.contains(channelId)) continue
            var index = tag.channelIds.size
            for (i in tag.channelIds.indices) {
                if (channelId < tag.channelIds[i]) {
                    index = i
                    break
                }
            }
            tag.channelIds.add(index, channelId)
            if (!modifiedFlag) modifiedFlag = true
        }
        if (modifiedFlag) save()
    }

    fun removeChannelAllTag(channelId: Int) {
        if (doRemoveChannelAllTag(channelId)) save()
    }

    // -------- private function BEGIN --------

    private fun doRemoveChannelAllTag(channelId: Int): Boolean {
        var flag = false
        for ((_, tag) in nameToTag) {
            if (tag.channelIds.remove(channelId) && !flag) flag = true
        }
        return flag
    }

    override fun load() {
        val path = persistentPath.resolve(PERSISTENT_NAME)
        nameToTag = if (!path.exists()) {
            HashMap()
        } else {
            val text = path.bufferedReader().use { it.readText() }
            convertJsonStringToTagMap(text)
        }

        for ((_, tag) in nameToTag) {
            val iter = tag.channelIds.iterator()
            while (iter.hasNext()) {
                val channelId = iter.next()
                if (!ChannelRepository.isValidChannelId(channelId)) {
                    iter.remove()
                }
            }
        }
    }

    private fun save() {
        val text = convertToJsonString()
        val path = persistentPath.resolve(PERSISTENT_NAME)
        path.bufferedWriter().use { it.write(text) }
    }

    private fun convertToJsonString(): String {
        val arr = JSONArray()
        for ((_, tag) in nameToTag) {
            arr.put(tag.convertToJson())
        }
        return arr.toString(4)
    }

    private fun convertJsonStringToTagMap(str: String): HashMap<String, Tag> {
        val map = HashMap<String, Tag>()
        val arr = JSONArray(str)
        for (i in 0 until arr.length()) {
            val tag = Tag.convertFromJson(arr.getJSONObject(i))
            map[tag.name] = tag
        }
        return map
    }

    // -------- private function END --------

}
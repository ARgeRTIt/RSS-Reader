package com.leaf.rssreader.repository

import com.leaf.rssreader.pojo.Detail

object DetailRepository {

    private val linkToDetail: MutableMap<String, Detail> = HashMap()
    private val linkQueue: MutableList<String> = mutableListOf()

    fun removeDetail(link: String) {
        linkToDetail.remove(link)
    }

    fun cacheDetail(link: String, detail: Detail) {
        if (linkToDetail.containsKey(link)) return
        if (linkQueue.size > 10) {
            removeDetail(linkQueue.removeFirst())
        }
        linkToDetail[link] = detail
        linkQueue.add(link)
    }

    fun getDetailByLink(link: String): Detail? {
        return linkToDetail[link]
    }

}
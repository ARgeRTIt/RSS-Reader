package com.leaf.rssreader

import com.leaf.rssreader.pojo.FullScreenDTO

typealias DrawerOpenFunc = (() -> Unit)?
typealias NavigateCallback = (String?) -> Unit
typealias FullScreenSetFunc = (FullScreenDTO) -> Unit

object Const {

    object ComposeArgument {
        const val LIST_COMPOSE_CHANNEL_ID = "id"
        const val DETAIL_COMPOSE_ID = "id"

        const val EDIT_CHANNEL_COMPOSE_CHANNEL_ID = "id"
        const val EDIT_TAG_COMPOSE_TAG_NAME = "name"
    }

}
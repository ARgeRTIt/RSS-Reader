package com.leaf.rssreader.ui

import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.border
import androidx.compose.foundation.combinedClickable
import androidx.compose.foundation.layout.*
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import com.leaf.rssreader.*
import com.leaf.rssreader.R
import com.leaf.rssreader.pojo.Tag
import com.leaf.rssreader.repository.ChannelRepository
import com.leaf.rssreader.repository.TagRepository

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun TagCompose(
    navigateCallback: NavigateCallback,
    drawerOpenFunc: DrawerOpenFunc
) {
    Scaffold(
        floatingActionButtonPosition = FabPosition.Center,
        floatingActionButton = {
            ExtendedFloatingActionButton(
                onClick = { navigateCallback(NavRoute.EDIT_TAG) }
            ) {
                Text(stringResource(id = R.string.add_tag))
            }
        }
    ) { paddingValues ->
        Column(
            modifier = Modifier
                .fillMaxSize()
                .padding(paddingValues)
        ) {
            Text(
                text = stringResource(id = R.string.click_tag_to_pull),
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(8.dp),
                textAlign = TextAlign.Center,
                style = MaterialTheme.typography.bodyLarge
            )

            FlowRow(
                modifier = Modifier
                    .fillMaxSize()
                    .padding(56.dp),
                horizontalArrangement = Arrangement.spacedBy(8.dp, Alignment.CenterHorizontally),
                verticalSpacing = 8.dp
            ) {
                for ((_, tag) in TagRepository.getAllTag()) {
                    CustomOutlinedButton(
                        navigateCallback = navigateCallback,
                        drawerOpenFunc = drawerOpenFunc,
                        tag = tag
                    )
                }
            }
        }
    }
}

@OptIn(ExperimentalFoundationApi::class)
@Composable
private fun CustomOutlinedButton(
    navigateCallback: NavigateCallback,
    drawerOpenFunc: DrawerOpenFunc,
    tag: Tag,
) {
    Text(
        text = tag.name,
        color = MaterialTheme.colorScheme.primary,
        modifier = Modifier
            .clip(ButtonDefaults.outlinedShape)
            .border(ButtonDefaults.outlinedButtonBorder, ButtonDefaults.outlinedShape)
            .combinedClickable(
                onClick = {
                    for (channelId in tag.channelIds) {
                        val url = ChannelRepository.getChannelUrl(channelId)
                        DownloadService.downloadFeed(
                            channelId = channelId,
                            url = url
                        )
                    }
                    if (null != drawerOpenFunc) drawerOpenFunc()
                },
                onLongClick = {
                    navigateCallback(
                        NavRoute.EDIT_TAG +
                                "?${Const.ComposeArgument.EDIT_TAG_COMPOSE_TAG_NAME}=" +
                                tag.name
                    )
                }
            )
            .padding(ButtonDefaults.ContentPadding)
    )
}
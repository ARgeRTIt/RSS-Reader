package com.leaf.rssreader.ui

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material.icons.filled.Delete
import androidx.compose.material.icons.filled.Done
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.lifecycle.viewmodel.initializer
import androidx.lifecycle.viewmodel.viewModelFactory
import com.leaf.rssreader.FullScreenSetFunc
import com.leaf.rssreader.NavigateCallback
import com.leaf.rssreader.R
import com.leaf.rssreader.pojo.FullScreenDTO
import com.leaf.rssreader.pojo.FullScreenDTOType
import com.leaf.rssreader.repository.ChannelRepository
import com.leaf.rssreader.repository.TagRepository
import com.leaf.rssreader.viewmodel.EditChannelViewModel

private typealias DescriptionState = EditChannelViewModel.DescriptionState
private typealias UrlState = EditChannelViewModel.UrlState

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun EditChannelCompose(
    navigateCallback: NavigateCallback,
    id: Int?,
    fullScreenSetFunc: FullScreenSetFunc,
    vm: EditChannelViewModel = viewModel(
        key = "edit-channel-$id",
        factory = viewModelFactory {
            initializer {
                EditChannelViewModel(id)
            }
        }
    )
) {

    if (null != vm.saveCallback) {
        Column(
            modifier = Modifier.fillMaxSize(),
            verticalArrangement = Arrangement.Center,
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Text(stringResource(id = R.string.saving))
        }
        return
    }

    Scaffold(
        bottomBar = {
            CenterAlignedTopAppBar(
                title = {
                    Text(stringResource(id = R.string.edit_channel))
                },
                navigationIcon = {
                    IconButton(onClick = { navigateCallback(null) }) {
                        Icon(Icons.Default.ArrowBack, "")
                    }
                },
                actions = {
                    if (null != id) {
                        IconButton(onClick = {
                            fullScreenSetFunc(FullScreenDTO(
                                type = FullScreenDTOType.REMOVE_CONFIRM,
                                confirmCallback = {
                                    TagRepository.removeChannelAllTag(id)
                                    ChannelRepository.removeChannel(id)
                                    navigateCallback(NavRoute.TAG) // 销毁残留的 compose
                                }
                            ) )
                        }) {
                            Icon(Icons.Default.Delete, "")
                        }
                    }

                    IconButton(onClick = {
                        vm.save { navigateCallback(NavRoute.TAG) }
                    }) {
                        Icon(Icons.Default.Done, "")
                    }
                }
            )
        }
    ) { paddingValues ->
        MainCompose(
            paddingValues = paddingValues,
            vm = vm
        )
    }

}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
private fun MainCompose(
    paddingValues: PaddingValues,
    vm: EditChannelViewModel
) {
    Column(
        modifier = Modifier
            .padding(paddingValues)
            .padding(32.dp, 8.dp)
            .fillMaxSize(),
        verticalArrangement = Arrangement.spacedBy(8.dp, Alignment.CenterVertically),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        val descriptionIsError = DescriptionState.NONE != vm.descriptionState

        TextField(
            label = {
                if (descriptionIsError)
                    Text(text = vm.descriptionState.str)
                else
                    Text(text = stringResource(id = R.string.description))
            },
            value = vm.description,
            onValueChange = {
                vm.description = it
                vm.descriptionState = DescriptionState.NONE
            },
            keyboardOptions = KeyboardOptions.Default.copy(
                imeAction = ImeAction.Next
            ),
            isError = descriptionIsError,
            modifier = Modifier.fillMaxWidth(),
            trailingIcon = if (descriptionIsError) {
                {
                    Icon(
                        painterResource(id = R.drawable.ic_baseline_error_24),
                        ""
                    )
                }
            } else null,
        )

        val urlIsError = UrlState.NONE != vm.urlState
        TextField(
            label = {
                if (urlIsError)
                    Text(text = vm.urlState.str)
                else
                    Text(text = stringResource(id = R.string.url))
            },
            value = vm.url,
            onValueChange = {
                vm.url = it.trim()
                vm.urlState = UrlState.NONE
            },
            keyboardOptions = KeyboardOptions.Default.copy(
                imeAction = ImeAction.Done
            ),
            isError = urlIsError,
            modifier = Modifier.fillMaxWidth(),
            trailingIcon = if (urlIsError) {
                {
                    Icon(
                        painterResource(id = R.drawable.ic_baseline_error_24),
                        ""
                    )
                }
            } else null,
        )

        val tagMap = TagRepository.getAllTag()
        if (tagMap.isNotEmpty()) {
            Text(
                text = stringResource(id = R.string.tag),
                modifier = Modifier.fillMaxWidth(),
                textAlign = TextAlign.Center,
                style = MaterialTheme.typography.titleLarge
            )

            FlowRow(
                modifier = Modifier.fillMaxWidth(),
                horizontalArrangement = Arrangement.spacedBy(8.dp)
            ) {
                for ((name, _) in tagMap) {
                    CustomToggleButton(
                        text = name,
                        checked = vm.tagSelectedList.contains(name)
                    ) {
                        vm.select(name)
                    }
                }
            }
        }
    }
}

@Composable
private fun CustomToggleButton(
    text: String,
    checked: Boolean,
    onClick: () -> Unit,
) {
    val colors = if (checked) {
        ButtonDefaults.buttonColors(
            containerColor = MaterialTheme.colorScheme.primary,
            contentColor = MaterialTheme.colorScheme.onPrimary
        )
    } else {
        ButtonDefaults.buttonColors(
            containerColor = MaterialTheme.colorScheme.surfaceVariant,
            contentColor = MaterialTheme.colorScheme.onSurfaceVariant
        )
    }

    Button(
        onClick = onClick,
        colors = colors
    ) {
        Text(text)
    }
}

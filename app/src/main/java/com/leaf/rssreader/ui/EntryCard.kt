package com.leaf.rssreader.ui

import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import com.leaf.rssreader.Util
import com.leaf.rssreader.pojo.Entry
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun EntryCard(
    entry: Entry,
    currentLocalDateTime: LocalDateTime,
    colors: CardColors,
    modifier: Modifier = Modifier,
    onclick: () -> Unit
) {
    val horizontalPadding = 8.dp
    val verticalPadding = 4.dp

    Card(
        onClick = onclick,
        colors = colors,
        modifier = modifier
    ) {
        Text(
            text = entry.title,
            style = MaterialTheme.typography.titleMedium,
            modifier = Modifier
                .padding(
                    PaddingValues(
                        horizontalPadding,
                        verticalPadding,
                        horizontalPadding,
                        0.dp
                    )
                )
                .fillMaxWidth()
        )

        Text(
            text = timestampToHumanReadingString(entry.pubDate, currentLocalDateTime),
            style = MaterialTheme.typography.bodyMedium,
            overflow = TextOverflow.Ellipsis,
            maxLines = 1,
            textAlign = TextAlign.End,
            modifier = Modifier
                .padding(
                    PaddingValues(
                        horizontalPadding,
                        verticalPadding,
                        horizontalPadding,
                        verticalPadding
                    )
                )
                .fillMaxWidth()
        )
    }
}

private fun timestampToHumanReadingString(
    second: Long,
    currentLocalDateTime: LocalDateTime
): String {
    val localDateTime = Util.timestampToLocalDateTime(second)
    if (localDateTime.year == currentLocalDateTime.year
        && localDateTime.dayOfYear == currentLocalDateTime.dayOfYear) {
        return localDateTime.format(DateTimeFormatter.ofPattern("HH:mm"))
    }
    return localDateTime.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm"))
}
package com.leaf.rssreader.ui

import androidx.activity.compose.BackHandler
import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material.icons.filled.Settings
import androidx.compose.material3.*
import androidx.compose.material3.windowsizeclass.WindowWidthSizeClass
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavHostController
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.currentBackStackEntryAsState
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import com.leaf.rssreader.*
import com.leaf.rssreader.R
import com.leaf.rssreader.repository.ChannelRepository
import com.leaf.rssreader.viewmodel.MainViewModel
import kotlinx.coroutines.launch

private const val LIST_COMPOSE_ROUTE = NavRoute.LIST +
        "/{${Const.ComposeArgument.LIST_COMPOSE_CHANNEL_ID}}"

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun MainCompose(
    widthSizeClass: WindowWidthSizeClass,
    vm: MainViewModel = viewModel()
) {
    val expandedScreenFlag = widthSizeClass == WindowWidthSizeClass.Expanded
    val navController = rememberNavController()
    val fullScreenSetFunc: FullScreenSetFunc = { fullScreenDTO ->
        vm.fullScreenDTO = fullScreenDTO
    }

    if (expandedScreenFlag) {
        PermanentNavigationDrawer(
            drawerContent = {
                PermanentDrawerSheet {
                    DrawerContent(vm, navController)
                }
            }
        ) {
            Content(
                navController = navController,
                fullScreenSetFunc = fullScreenSetFunc,
                drawerOpenFunc = null
            )
        }
    } else {
        val drawerState = rememberDrawerState(initialValue = DrawerValue.Closed)
        val coroutineScope = rememberCoroutineScope()
        BackHandler(enabled = drawerState.isOpen) {
            coroutineScope.launch {
                drawerState.close()
            }
        }
        ModalNavigationDrawer(
            drawerState = drawerState,
            drawerContent = {
                ModalDrawerSheet {
                    DrawerContent(vm, navController) {
                        coroutineScope.launch { drawerState.close() }
                    }
                }
            }
        ) {
            Content(
                navController = navController,
                fullScreenSetFunc = fullScreenSetFunc
            ) {
                coroutineScope.launch { drawerState.open() }
            }
        }
    }

    if (null != vm.fullScreenDTO) {
        FullScreen(vm.fullScreenDTO!!) { vm.fullScreenDTO = null }
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
private fun DrawerContent(
    vm: MainViewModel,
    navController: NavHostController,
    clickExtraCallback: (() -> Unit)? = null
) {
    val navBackStackEntry by navController.currentBackStackEntryAsState()
    val currentRoute = navBackStackEntry?.destination?.route ?: NavRoute.TAG

    Row(
        modifier = Modifier.fillMaxWidth(),
        horizontalArrangement = Arrangement.SpaceBetween,
        verticalAlignment = Alignment.CenterVertically
    ) {
        IconButton(onClick = {
            navigate(navController, NavRoute.SETTING)
            clickExtraCallback?.invoke()
        }) {
            Icon(Icons.Default.Settings, stringResource(id = R.string.setting))
        }

        IconButton(onClick = {
            navigate(navController, NavRoute.SORT_CHANNEL)
            clickExtraCallback?.invoke()
        }) {
            Icon(painterResource(id = R.drawable.ic_baseline_sort_24), "sort")
        }

        IconButton(onClick = {
            navigate(navController, NavRoute.EDIT_CHANNEL)
            clickExtraCallback?.invoke()
        }) {
            Icon(Icons.Default.Add, "add")
        }
    }

    CustomDivider()

    NavigationDrawerItem(
        label = {
            Text(
                text = stringResource(id = R.string.all_tag),
                modifier = Modifier.fillMaxWidth(),
                textAlign = TextAlign.Center
            )
        },
        selected = NavRoute.TAG == currentRoute,
        onClick = {
            navigate(navController, NavRoute.TAG)
            clickExtraCallback?.invoke()
        }
    )

    val channelList by Util.generateLifecycleAwareFlow(
        ChannelRepository.channelListStateFlow
    ).collectAsState(initial = emptyList())

    CustomLazyColumn {
        items(
            items = channelList,
            key = { it.id },
            contentType = { it.javaClass }
        ) { channel ->
            val channelId = channel.id
            CustomNavigationDrawerItem(
                selected = LIST_COMPOSE_ROUTE == currentRoute
                        && navBackStackEntry?.arguments?.getInt(
                    Const.ComposeArgument.LIST_COMPOSE_CHANNEL_ID
                ) == channelId,
                description = channel.description,
                onClick = {
                    navigate(navController, "${NavRoute.LIST}/$channelId")
                    clickExtraCallback?.invoke()
                },
                onLongPress = {
                    navigate(
                        navController,
                        NavRoute.EDIT_CHANNEL +
                                "?${Const.ComposeArgument.EDIT_CHANNEL_COMPOSE_CHANNEL_ID}=" +
                                channelId
                    )
                    clickExtraCallback?.invoke()
                }
            ) {
                if (vm.downloadingUrls.contains(channel.url)) {
                    Text(
                        text = stringResource(id = R.string.pulling),
                        maxLines = 1
                    )
                    return@CustomNavigationDrawerItem
                }
                val unreadList by Util.generateLifecycleAwareFlow(
                    flow = ChannelRepository.getUnreadEntriesByChannelId(channel.id)
                ).collectAsState(initial = emptyList())

                val unreadSize = unreadList.size
                if (unreadSize <= 0) return@CustomNavigationDrawerItem
                Text(
                    text = unreadSize.toString(),
                    maxLines = 1
                )
            }
        }
    }

}

@Composable
private fun Content(
    navController: NavHostController,
    fullScreenSetFunc: FullScreenSetFunc,
    drawerOpenFunc: DrawerOpenFunc?
) {
    val navigateCallback = fun (route: String?) {
        if (null == route) {
            navController.navigateUp()
            return
        }
        navigate(navController, route)
    }

    NavHost(
        navController = navController,
        startDestination = NavRoute.TAG
    ) {
        composable(NavRoute.TAG) {
            TagCompose(navigateCallback, drawerOpenFunc)
        }
        composable(
            route = LIST_COMPOSE_ROUTE,
            arguments = listOf(
                navArgument(Const.ComposeArgument.LIST_COMPOSE_CHANNEL_ID) {
                    type = NavType.IntType
                }
            )
        ) {
            val id = it.arguments!!.getInt(Const.ComposeArgument.LIST_COMPOSE_CHANNEL_ID)
            ListCompose(navigateCallback, drawerOpenFunc, id)
        }
        composable(NavRoute.SETTING) {
            SettingCompose(navigateCallback, fullScreenSetFunc)
        }
        composable(
            route = "${NavRoute.DETAIL}/{${Const.ComposeArgument.DETAIL_COMPOSE_ID}}",
            arguments = listOf(
                navArgument(Const.ComposeArgument.DETAIL_COMPOSE_ID) {
                    type = NavType.StringType
                }
            )
        ) {
            val id = it.arguments?.getString(Const.ComposeArgument.DETAIL_COMPOSE_ID)
                ?: return@composable
            DetailCompose(navigateCallback, id, fullScreenSetFunc)
        }

        composable(
            route = NavRoute.EDIT_CHANNEL +
                    "?${Const.ComposeArgument.EDIT_CHANNEL_COMPOSE_CHANNEL_ID}" +
                    "={${Const.ComposeArgument.EDIT_CHANNEL_COMPOSE_CHANNEL_ID}}",
            arguments = listOf(
                navArgument(
                    Const.ComposeArgument.EDIT_CHANNEL_COMPOSE_CHANNEL_ID
                ) {
                    type = NavType.StringType
                    nullable = true
                }
            ),
        ) {
            val id = it.arguments?.getString(Const.ComposeArgument.EDIT_CHANNEL_COMPOSE_CHANNEL_ID)
            EditChannelCompose(navigateCallback, id?.toInt(), fullScreenSetFunc)
        }
        composable(
            route = NavRoute.EDIT_TAG +
                    "?${Const.ComposeArgument.EDIT_TAG_COMPOSE_TAG_NAME}" +
                    "={${Const.ComposeArgument.EDIT_TAG_COMPOSE_TAG_NAME}}",
            arguments = listOf(
                navArgument(Const.ComposeArgument.EDIT_TAG_COMPOSE_TAG_NAME) {
                    type = NavType.StringType
                    nullable = true
                }
            )
        ) {
            val name = it.arguments?.getString(Const.ComposeArgument.EDIT_TAG_COMPOSE_TAG_NAME)
            EditTagCompose(navigateCallback, name, fullScreenSetFunc)
        }

        composable(
            route = NavRoute.SORT_CHANNEL
        ) {
            SortChannelCompose()
        }
        composable(
            route = NavRoute.LOG
        ) {
            LogCompose()
        }
    }
}

@Composable
private fun CustomDivider() {
    Divider(
        modifier = Modifier.padding(0.dp, 4.dp, 0.dp, 4.dp),
        color = MaterialTheme.colorScheme.outline
    )
}

@OptIn(ExperimentalMaterial3Api::class, ExperimentalFoundationApi::class)
@Composable
private fun CustomNavigationDrawerItem(
    selected: Boolean,
    description: String,
    onClick: () -> Unit,
    onLongPress: () -> Unit,
    trailingCompose: @Composable () -> Unit
) {
    Row(
        verticalAlignment = Alignment.CenterVertically,
        horizontalArrangement = Arrangement.SpaceBetween,
        modifier = Modifier
            .height(56.dp)
            .fillMaxWidth()
            .clip(CircleShape)
            .background(
                color = surfaceColorAtElevation(
                    NavigationDrawerItemDefaults
                        .colors()
                        .containerColor(selected).value,
                    LocalAbsoluteTonalElevation.current
                ),
                shape = CircleShape
            )
            .combinedClickable(
                onClick = onClick,
                onLongClick = onLongPress
            )
            .padding(start = 16.dp, end = 24.dp)
    ) {
        Text(
            text = description,
            maxLines = 1,
            overflow = TextOverflow.Ellipsis,
            modifier = Modifier.weight(1f, false)
        )

        trailingCompose()
    }
}

// 从 Surface.kt 中复制过来
@Composable
private fun surfaceColorAtElevation(color: Color, elevation: Dp): Color {
    return if (color == MaterialTheme.colorScheme.surface) {
        MaterialTheme.colorScheme.surfaceColorAtElevation(elevation)
    } else {
        color
    }
}

package com.leaf.rssreader.ui

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material.icons.filled.Delete
import androidx.compose.material.icons.filled.Done
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.lifecycle.viewmodel.initializer
import androidx.lifecycle.viewmodel.viewModelFactory
import com.leaf.rssreader.FullScreenSetFunc
import com.leaf.rssreader.NavigateCallback
import com.leaf.rssreader.R
import com.leaf.rssreader.Util
import com.leaf.rssreader.pojo.FullScreenDTO
import com.leaf.rssreader.pojo.FullScreenDTOType
import com.leaf.rssreader.repository.ChannelRepository
import com.leaf.rssreader.repository.TagRepository
import com.leaf.rssreader.viewmodel.EditTagViewModel

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun EditTagCompose(
    navigateCallback: NavigateCallback,
    name: String?,
    fullScreenSetFunc: FullScreenSetFunc,
    vm: EditTagViewModel = viewModel(
        key = "edit-tag-$name",
        factory = viewModelFactory {
            initializer {
                EditTagViewModel(name)
            }
        }
    )
) {
    Scaffold(
        bottomBar = {
            CenterAlignedTopAppBar(
                title = {
                    Text(stringResource(id = R.string.edit_tag))
                },
                navigationIcon = {
                    IconButton(onClick = { navigateCallback(null) }) {
                        Icon(Icons.Default.ArrowBack, "")
                    }
                },
                actions = {
                    if (null != name) {
                        IconButton(onClick = {
                            fullScreenSetFunc(
                                FullScreenDTO(
                                    type = FullScreenDTOType.REMOVE_CONFIRM,
                                    confirmCallback = {
                                        TagRepository.removeTag(name)
                                        navigateCallback(null)
                                    }
                                )
                            )
                        }) {
                            Icon(Icons.Default.Delete, "")
                        }
                    }

                    IconButton(onClick = {
                        if (vm.save()) navigateCallback(null)
                    }) {
                        Icon(Icons.Default.Done, "")
                    }
                }
            )
        }
    ) { paddingValues ->
        MainCompose(
            paddingValues = paddingValues,
            vm = vm
        )
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
private fun MainCompose(
    paddingValues: PaddingValues,
    vm: EditTagViewModel
) {
    Column(
        verticalArrangement = Arrangement.spacedBy(8.dp),
        modifier = Modifier
            .padding(paddingValues)
            .padding(16.dp)
            .fillMaxSize()
    ) {
        val isNameError = EditTagViewModel.NameState.NONE != vm.nameState
        TextField(
            label = {
                Text(stringResource(id = R.string.name))
            },
            value = vm.name,
            onValueChange = {
                vm.name = it
                vm.nameState = EditTagViewModel.NameState.NONE
            },
            modifier = Modifier.fillMaxWidth(),
            isError = isNameError,
            trailingIcon = {
                if (!isNameError) return@TextField
                Icon(painterResource(id = R.drawable.ic_baseline_error_24), "")
            },
            singleLine = true,
            keyboardOptions = KeyboardOptions.Default.copy(
                imeAction = ImeAction.Done
            )
        )

        val channelList by Util.generateLifecycleAwareFlow(
            ChannelRepository.channelListStateFlow
        ).collectAsState(initial = emptyList())

        if (channelList.isNotEmpty()) {
            Text(
                text = stringResource(id = R.string.channel),
                modifier = Modifier.fillMaxWidth(),
                textAlign = TextAlign.Center,
                style = MaterialTheme.typography.titleLarge
            )

            CustomLazyColumn {
                items(
                    items = channelList,
                    key = { it.id },
                    contentType = { it.javaClass }
                ) {
                    val channelId = it.id
                    Row(
                        verticalAlignment = Alignment.CenterVertically,
                        horizontalArrangement = Arrangement.SpaceBetween,
                        modifier = Modifier
                            .fillMaxWidth()
                            .clickable { vm.select(channelId) }
                            .padding(0.dp, 8.dp)
                    ) {
                        Text(
                            text = it.description,
                            maxLines = 1,
                            overflow = TextOverflow.Ellipsis,
                            modifier = Modifier.weight(1f, false)
                        )

                        RadioButton(
                            selected = vm.channelSelectedList.contains(channelId),
                            onClick = null
                        )
                    }
                }
            }
        }
    }
}
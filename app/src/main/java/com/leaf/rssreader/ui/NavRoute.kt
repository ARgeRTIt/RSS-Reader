package com.leaf.rssreader.ui

import androidx.navigation.NavController
import androidx.navigation.NavGraph.Companion.findStartDestination

object NavRoute {
    const val TAG = "tag"
    const val LIST = "list"
    const val SETTING = "setting"
    const val DETAIL = "detail"

    const val EDIT_CHANNEL = "edit_channel"
    const val EDIT_TAG = "edit_tag"

    const val SORT_CHANNEL = "sort_channel"
    const val LOG = "log"
}

fun navigate(
    navController: NavController,
    route: String,
    initializationFlag: Boolean = false
) {
    navController.navigate(route) {
        launchSingleTop = true
        if (initializationFlag) {
            popUpTo(navController.graph.findStartDestination().id)
        } else {
            val coreRoute = extractRoute(route)
            var destinationId: Int? = null
            for (tempDestination in navController.graph) {
                if (coreRoute == extractRoute(tempDestination.route!!)) {
                    destinationId = tempDestination.id
                    break
                }
            }
            if (null != destinationId) popUpTo(destinationId) { inclusive = true }
        }
    }
}

private fun extractRoute(route: String): String {
    return route.substringBefore('/').substringBefore('?')
}
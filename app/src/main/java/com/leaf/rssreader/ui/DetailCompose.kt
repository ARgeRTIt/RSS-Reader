package com.leaf.rssreader.ui

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyListScope
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.text.selection.SelectionContainer
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.lifecycle.viewmodel.initializer
import androidx.lifecycle.viewmodel.viewModelFactory
import coil.compose.AsyncImage
import com.leaf.rssreader.FullScreenSetFunc
import com.leaf.rssreader.HtmlParser
import com.leaf.rssreader.NavigateCallback
import com.leaf.rssreader.R
import com.leaf.rssreader.pojo.Detail
import com.leaf.rssreader.pojo.FullScreenDTO
import com.leaf.rssreader.pojo.FullScreenDTOType
import com.leaf.rssreader.repository.SettingRepository
import com.leaf.rssreader.viewmodel.DetailViewModel

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun DetailCompose(
    navigateCallback: NavigateCallback,
    tempId: String,
    fullScreenSetFunc: FullScreenSetFunc,
    vm: DetailViewModel = viewModel(
        factory = viewModelFactory {
            initializer {
                DetailViewModel(tempId)
            }
        }
    )
) {
    val context = LocalContext.current

    Scaffold(
        bottomBar = {
            CenterAlignedTopAppBar(
                title = {
                    Text(stringResource(id = R.string.detail))
                },
                navigationIcon = {
                    IconButton(onClick = { navigateCallback(null) }) {
                        Icon(Icons.Default.ArrowBack, "")
                    }
                },
                actions = {
                    if (DetailViewModel.State.NORMAL == vm.state) {
                        TextButton(onClick = {
                            vm.pullFullHTML()
                        }) {
                            Text(stringResource(id = R.string.full))
                        }
                    } else if (DetailViewModel.State.FULL == vm.state
                        || DetailViewModel.State.FULL_PULLING_FAIL == vm.state) {
                        TextButton(onClick = {
                            vm.state = DetailViewModel.State.NORMAL
                        }) {
                            Text(stringResource(id = R.string.simple))
                        }
                    }
                }
            )
        }
    ) {
        SelectionContainer {
            CompositionLocalProvider(
                LocalTextStyle provides MaterialTheme.typography.bodyLarge
            ) {
                LazyColumn(
                    modifier = Modifier.padding(it),
                    verticalArrangement = Arrangement.spacedBy(18.dp),
                    contentPadding = PaddingValues(16.dp, 0.dp)
                ) {
                    when (vm.state) {
                        DetailViewModel.State.NORMAL -> {
                            detail(
                                vm = vm,
                                canShowImage = SettingRepository.canShowDescriptionImage(context),
                                fullScreenSetFunc = fullScreenSetFunc
                            )
                        }
                        else -> {
                            fullDetail(
                                vm = vm,
                                canShowImage = SettingRepository.canShowFullContentImage(context),
                                fullScreenSetFunc = fullScreenSetFunc
                            )
                        }
                    }
                }
            }
        }
    }
}

private fun LazyListScope.detail(
    vm: DetailViewModel,
    canShowImage: Boolean,
    fullScreenSetFunc: FullScreenSetFunc
){
    head(vm)
    body(vm.descriptionDetail, canShowImage, fullScreenSetFunc)
}

private fun LazyListScope.fullDetail(
    vm: DetailViewModel,
    canShowImage: Boolean,
    fullScreenSetFunc: FullScreenSetFunc
) {
    head(vm)

    if (DetailViewModel.State.FULL_PULLING == vm.state) {
        item {
            Text(
                text = stringResource(id = R.string.pulling),
                modifier = Modifier.fillMaxSize(),
                textAlign = TextAlign.Center
            )
        }
        return
    }
    if (DetailViewModel.State.FULL_PULLING_FAIL == vm.state) {
        item {
            Text(
                text = stringResource(id = R.string.url_cannot_access),
                modifier = Modifier.fillMaxSize(),
                textAlign = TextAlign.Center
            )
        }
        return
    }

    body(vm.detail!!, canShowImage, fullScreenSetFunc)
}

private fun LazyListScope.body(
    detail: Detail,
    canShowImage: Boolean,
    fullScreenSetFunc: FullScreenSetFunc
) {
    items(items = detail.contentList) {
        val tag = it.first
        val value = it.second
        when (tag) {
            HtmlParser.ValidHtmlTagEnum.P.str -> {
                Text(value)
            }
            HtmlParser.ValidHtmlTagEnum.IMG.str -> {
                val url = value.text
                if (!canShowImage) {
                    Text(url)
                } else {
                    AsyncImage(
                        model = url,
                        contentDescription = "",
                        modifier = Modifier
                            .fillMaxWidth()
                            .clickable {
                                fullScreenSetFunc(
                                    FullScreenDTO(
                                        type = FullScreenDTOType.IMAGE,
                                        data = url
                                    )
                                )
                            },
                        contentScale = ContentScale.FillWidth
                    )
                }
            }
            HtmlParser.ValidHtmlTagEnum.LI.str -> {
                Text("• $value")
            }
            HtmlParser.ValidHtmlTagEnum.H.str -> {
                Text(
                    text = value,
                    style = MaterialTheme.typography.titleLarge
                )
            }
        }
    }
}

private fun LazyListScope.head(vm: DetailViewModel) {
    item {
        Text(
            text = vm.entry.title,
            style = MaterialTheme.typography.headlineSmall
        )
    }

    item {
        Divider()
    }

    item {
        Text(
            text = vm.pubDateStr,
            style = MaterialTheme.typography.labelMedium
        )
    }

    item {
        Divider()
    }
}
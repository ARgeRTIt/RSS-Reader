package com.leaf.rssreader.ui

import android.content.Context
import android.net.Uri
import android.provider.MediaStore
import androidx.activity.compose.ManagedActivityResultLauncher
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.documentfile.provider.DocumentFile
import com.leaf.restorelibrary.InitAndRestoreService
import com.leaf.rssreader.FullScreenSetFunc
import com.leaf.rssreader.NavigateCallback
import com.leaf.rssreader.R
import com.leaf.rssreader.Util
import com.leaf.rssreader.pojo.FullScreenDTO
import com.leaf.rssreader.pojo.FullScreenDTOType
import com.leaf.rssreader.pojo.Setting
import com.leaf.rssreader.repository.SettingRepository
import kotlinx.coroutines.launch

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun SettingCompose(
    navigateCallback: NavigateCallback,
    fullScreenSetFunc: FullScreenSetFunc
) {
    val snackbarHostState = remember { SnackbarHostState() }
    val coroutineScope = rememberCoroutineScope()
    val context = LocalContext.current

    Scaffold(
        snackbarHost = { SnackbarHost(hostState = snackbarHostState) },
        bottomBar = {
            CenterAlignedTopAppBar(
                title = {
                    Text(stringResource(id = R.string.setting))
                },
                navigationIcon = {
                    IconButton(onClick = { navigateCallback(null) }) {
                        Icon(Icons.Default.ArrowBack, "")
                    }
                }
            )
        }
    ) { paddingValues ->
        Column(
            modifier = Modifier.padding(paddingValues)
        ) {
            val setting by Util.generateLifecycleAwareFlow(
                SettingRepository.settingStateFlow
            ).collectAsState(Setting())

            SettingRow(
                description = stringResource(id = R.string.mobile_network_show_description_image),
                onClick = {
                    SettingRepository.setMobileNetworkDescriptionImageShowFlag(
                        !setting.mobileNetworkDescriptionImageShow
                    )
                }
            ) {
                Switch(
                    checked = setting.mobileNetworkDescriptionImageShow,
                    onCheckedChange = null
                )
            }

            SettingRow(
                description = stringResource(id = R.string.mobile_network_show_full_content_image),
                onClick = {
                    SettingRepository.setMobileNetworkFullContentImageShowFlag(
                        !setting.mobileNetworkFullContentImageShow
                    )
                }
            ) {
                Switch(
                    checked = setting.mobileNetworkFullContentImageShow,
                    onCheckedChange = null
                )
            }

            SettingRow(
                description = stringResource(id = R.string.read_entries_cached_days),
                onClick = {
                    fullScreenSetFunc(
                        FullScreenDTO(
                            type = FullScreenDTOType.PICK_READ_ENTRIES_CACHED_DAYS,
                            data = SettingRepository.settingStateFlow.value.readEntriesCachedDays
                        ) {
                            it ?: return@FullScreenDTO
                            SettingRepository.setReadEntriesCachedDays(it as Int)
                        }
                    )
                }
            ) {
                Text(setting.readEntriesCachedDays.toString())
            }

            SettingRow(
                description = stringResource(id = R.string.font_scale_extra_multiply_by_10),
                onClick = {
                    fullScreenSetFunc(
                        FullScreenDTO(
                            type = FullScreenDTOType.PICK_FONT_SCALE_EXTRA,
                            data = SettingRepository
                                .settingStateFlow
                                .value
                                .fontScaleExtraMultiplyBy10
                        ) {
                            it ?: return@FullScreenDTO
                            SettingRepository.setFontScaleExtraMultiplyBy10(it as Int)
                        }
                    )
                }
            ) {
                Text(setting.fontScaleExtraMultiplyBy10.toString())
            }

            val backupLauncher = openBackupLauncher()
            SettingRow(
                description = stringResource(id = R.string.backup),
                onClick = {
                    backupLauncher.launch(MediaStore.Downloads.INTERNAL_CONTENT_URI)
                }
            )

            val restoreLauncher = openRestoreLauncher(context) {
                coroutineScope.launch {
                    snackbarHostState.showSnackbar(it)
                }
            }
            SettingRow(
                description = stringResource(id = R.string.restore),
                onClick = {
                    restoreLauncher.launch(MediaStore.Downloads.INTERNAL_CONTENT_URI)
                }
            )

            SettingRow(
                description = "log",
                onClick = {
                    navigateCallback(NavRoute.LOG)
                }
            )
        }
    }
}

@Composable
private fun openBackupLauncher(): ManagedActivityResultLauncher<Uri?, Uri?> {
    val context = LocalContext.current
    return rememberLauncherForActivityResult(
        contract = ActivityResultContracts.OpenDocumentTree()
    ) { baseUri ->
        if (null == baseUri) return@rememberLauncherForActivityResult
        val rootDocumentDirectory = DocumentFile.fromTreeUri(context, baseUri)!!
        InitAndRestoreService.backup(context, rootDocumentDirectory)
    }
}

@Composable
private fun openRestoreLauncher(
    context: Context,
    showSnackbar: (String) -> Unit
): ManagedActivityResultLauncher<Uri?, Uri?> {
    return rememberLauncherForActivityResult(
        contract = ActivityResultContracts.OpenDocumentTree()
    ) { baseUri ->
        if (null == baseUri) {
            return@rememberLauncherForActivityResult
        }

        val rootDocumentFile = DocumentFile.fromTreeUri(context, baseUri)!!
        if (InitAndRestoreService.restore(context, rootDocumentFile))
            showSnackbar("restore success")
        else
            showSnackbar("restore error")
    }
}

@Composable
private fun SettingRow(
    description: String,
    onClick: () -> Unit,
    remanentContent: (@Composable () -> Unit)? = null
) {
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .clickable(onClick = onClick)
            .padding(12.dp),
        horizontalArrangement = Arrangement.SpaceBetween,
        verticalAlignment = Alignment.CenterVertically
    ) {
        Text(
            text = description,
            modifier = Modifier.weight(0.8f, false)
        )
        remanentContent?.let {
            Box(
                modifier = Modifier.weight(0.2f, false),
                contentAlignment = Alignment.TopEnd
            ) {
                remanentContent()
            }
        }
    }
}
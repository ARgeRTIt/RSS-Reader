package com.leaf.rssreader.ui

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.KeyboardArrowDown
import androidx.compose.material.icons.filled.KeyboardArrowUp
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import com.leaf.rssreader.R
import com.leaf.rssreader.Util
import com.leaf.rssreader.repository.ChannelRepository
import com.leaf.rssreader.viewmodel.SortChannelViewModel

@Composable
fun SortChannelCompose(
    vm: SortChannelViewModel = viewModel()
) {
    val channelList by Util.generateLifecycleAwareFlow(
        ChannelRepository.channelListStateFlow
    ).collectAsState(initial = emptyList())

    CustomLazyColumn(
        contentPadding = PaddingValues(8.dp)
    ) {
        itemsIndexed(
            items = channelList,
            key = { _, channel -> channel.id },
            contentType = { _, channel -> channel.javaClass }
        ) { index, channel ->
            Row(
                modifier = Modifier.fillMaxWidth(),
                horizontalArrangement = Arrangement.SpaceBetween,
                verticalAlignment = Alignment.CenterVertically
            ) {
                Text(
                    text = channel.description,
                    maxLines = 1,
                    overflow = TextOverflow.Ellipsis,
                    modifier = Modifier.weight(1f, false),
                    color = if (channel.id == vm.channelIdselected)
                        MaterialTheme.colorScheme.outline
                    else
                        Color.Unspecified
                )

                Row {
                    val channelId = channel.id
                    if (channelId == vm.channelIdselected) {
                        TextButton(onClick = {
                            vm.cancelSelect()
                        }) {
                            Text(stringResource(id = R.string.cancel))
                        }
                    } else {
                        TextButton(onClick = {
                            vm.channelIdselected = channelId
                        }) {
                            Text(stringResource(id = R.string.select))
                        }

                        if (ChannelRepository.isChannelIdValid(vm.channelIdselected)) {
                            val frontIndex = index - 1
                            if (frontIndex < 0
                                || channelList[frontIndex].id != vm.channelIdselected
                            ) {
                                IconButton(
                                    onClick = {
                                        vm.changePosition(channelId, true)
                                    },
                                    colors = IconButtonDefaults.iconButtonColors(
                                        contentColor = MaterialTheme.colorScheme.primary
                                    )
                                ) {
                                    Icon(Icons.Default.KeyboardArrowUp, "")
                                }
                            }

                            val behindIndex = index + 1
                            if (behindIndex >= channelList.size
                                || channelList[behindIndex].id != vm.channelIdselected
                            ) {
                                IconButton(
                                    onClick = {
                                        vm.changePosition(channelId, false)
                                    },
                                    colors = IconButtonDefaults.iconButtonColors(
                                        contentColor = MaterialTheme.colorScheme.primary
                                    )
                                ) {
                                    Icon(Icons.Default.KeyboardArrowDown, "")
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
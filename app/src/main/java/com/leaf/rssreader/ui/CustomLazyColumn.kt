package com.leaf.rssreader.ui

import android.content.res.Configuration
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyListScope
import androidx.compose.foundation.lazy.LazyListState
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.KeyboardArrowDown
import androidx.compose.material.icons.filled.KeyboardArrowUp
import androidx.compose.material3.Icon
import androidx.compose.runtime.*
import androidx.compose.runtime.saveable.Saver
import androidx.compose.runtime.saveable.listSaver
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.input.nestedscroll.NestedScrollConnection
import androidx.compose.ui.input.nestedscroll.NestedScrollSource
import androidx.compose.ui.input.nestedscroll.nestedScroll
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.unit.Density
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.Velocity
import androidx.compose.ui.unit.dp
import kotlin.math.roundToInt

@Composable
fun CustomLazyColumn(
    modifier: Modifier = Modifier,
    contentPadding: PaddingValues = PaddingValues(0.dp),
    verticalArrangement: Arrangement.Vertical = Arrangement.Top,
    content: LazyListScope.() -> Unit
) {
    val state = rememberLazyListState()
    val customNestedScrollState = rememberCustomNestedScrollState()

    Column( // 让 LazyColumn 能使用 Modifier.weight
        modifier = modifier
            .fillMaxSize()
            .nestedScroll(CustomNestedScrollConnection(customNestedScrollState))
    ) {
        val channelShowState by rememberContentState(state)

        CustomColumnScrollGuide(
            isUp = true,
            isShow = ContentState.ALL != channelShowState
                    && ContentState.TOP != channelShowState
        )

        LazyColumn(
            state = state,
            modifier = Modifier
                .fillMaxHeight()
                .weight(1f, false)
                .padding(
                    top = convertPixelToDp(customNestedScrollState.topHeight),
                    //bottom = convertPixelToDp(customNestedScrollState.bottomHeight)
                ),
            contentPadding = contentPadding,
            verticalArrangement = verticalArrangement,
        ) {
            content()

            item(
                contentType = "item-end-space"
            ) {
                Spacer(
                    modifier = Modifier
                        .fillMaxWidth()
                        .height(
                            (LocalConfiguration.current.screenHeightDp / 4).dp
                        )
                )
            }
        }

        CustomColumnScrollGuide(
            isUp = false,
            isShow = ContentState.ALL != channelShowState
                    && ContentState.BOTTOM != channelShowState
        )
    }
}

@Composable
private fun convertPixelToDp(pixel: Float): Dp {
    return (pixel / LocalContext.current.resources.displayMetrics.density).dp
}

@Composable
private fun rememberContentState(
    lazyListState: LazyListState
): State<ContentState> {
    return remember {
        derivedStateOf {
            val layoutInfo = lazyListState.layoutInfo
            val visibleItemsInfo = layoutInfo.visibleItemsInfo
            if (layoutInfo.totalItemsCount == 0) {
                return@derivedStateOf ContentState.ALL
            }

            val lastVisibleItem = visibleItemsInfo.last()
            val halfItemHeight = lastVisibleItem.size / 2
            val isLastVisibleItemShowHalf =
                layoutInfo.viewportEndOffset - lastVisibleItem.offset >= halfItemHeight
            val isFirstVisibleItemShowHalf =
                lazyListState.firstVisibleItemScrollOffset < halfItemHeight

            if (layoutInfo.totalItemsCount == visibleItemsInfo.size
                && isFirstVisibleItemShowHalf
                && isLastVisibleItemShowHalf) {
                return@derivedStateOf ContentState.ALL
            }
            if (lazyListState.firstVisibleItemIndex == 0
                && isFirstVisibleItemShowHalf) {
                return@derivedStateOf ContentState.TOP
            }
            if (lastVisibleItem.index == layoutInfo.totalItemsCount - 1
                && isLastVisibleItemShowHalf) {
                return@derivedStateOf ContentState.BOTTOM
            }
            ContentState.MIDDLE
        }
    }
}

private enum class ContentState {
    ALL,
    TOP,
    MIDDLE,
    BOTTOM
}

@Composable
private fun CustomColumnScrollGuide(isUp: Boolean, isShow: Boolean) {
    Row(
        horizontalArrangement = Arrangement.Center,
        modifier = Modifier
            .fillMaxWidth()
            .height(16.dp)
    ) {
        if (isShow) {
            Icon(
                if (isUp) Icons.Default.KeyboardArrowUp else Icons.Default.KeyboardArrowDown,
                ""
            )
        }
    }
}

@Composable
private fun rememberCustomNestedScrollState(): CustomNestedScrollState {
    val localDensity = LocalDensity.current
    val localConfiguration = LocalConfiguration.current
    return rememberSaveable(
        saver = getSaver(localDensity, localConfiguration)
    ) {
        CustomNestedScrollState(
            maxHeightPx = with(localDensity) { localConfiguration.screenHeightDp.dp.toPx() }
        )
    }
}

private class CustomNestedScrollConnection(
    private val state: CustomNestedScrollState
): NestedScrollConnection {
    override fun onPostScroll(
        consumed: Offset,
        available: Offset,
        source: NestedScrollSource
    ): Offset {
        val y = available.y
        if (y == 0f) return Offset.Zero
        if (source != NestedScrollSource.Drag) return Offset.Zero
        return Offset(0f, state.onPostScroll(y, consumed.y))
    }

    override fun onPreScroll(available: Offset, source: NestedScrollSource): Offset {
        val y = available.y
        if (y == 0f) return Offset.Zero
        return Offset(0f, state.onPreScroll(y, source == NestedScrollSource.Fling))
    }

    override suspend fun onPreFling(available: Velocity): Velocity {
        state.onRelease()
        return super.onPreFling(available)
    }
}


private class CustomNestedScrollState(
    maxHeightPx: Float,
    topScrolledPercent: Float = 0f,
//    bottomScrolledPercent: Float = 0f,
) {
    private val topScrolledData = ScrolledData(
        maxValue = maxHeightPx / 2f,
        valuePercent = topScrolledPercent
    )
//    private val bottomScrolledData = ScrolledData(
//        maxValue = -maxHeightPx / 4f,
//        valuePercent = bottomScrolledPercent
//    )
    val topHeight get() = topScrolledData.value
//    val bottomHeight get() = -bottomScrolledData.value

    fun getTopScrolledPercent() = topScrolledData.getValuePercent()
//    fun getBottomScrolledPercent() = bottomScrolledData.getValuePercent()

    fun onPostScroll(delta: Float, consumed: Float): Float {
        return if (delta > 0) {
            topScrolledData.onPostScroll(delta)
        } else if (delta < 0) {
            // TODO 列表底部往上滑，不跟手
            // Column 维持显示最底部需要消费部分 delta
            // (加上 consumed 后，效果好了一点，但还是不跟手)
//            bottomScrolledData.onPostScroll(delta, consumed)
            0f
        } else {
            0f
        }
    }

    fun onPreScroll(delta: Float, isFling: Boolean): Float {
        return if (delta > 0) {
//            bottomScrolledData.onPreScroll(delta, isFling)
            0f
        } else if (delta < 0) {
            topScrolledData.onPreScroll(delta, isFling)
        } else {
            0f
        }
    }

    fun onRelease() {
        topScrolledData.onRelease()
//        bottomScrolledData.onRelease()
    }

    private class ScrolledData(
        val maxValue: Float,
        valuePercent: Float
    ) {
        var value by mutableStateOf(maxValue * valuePercent)
        var expanded: Boolean = valuePercent > THRESHOLD_FACTOR
        private val threshold = maxValue * THRESHOLD_FACTOR

        fun getValuePercent(): Float {
            return (value / maxValue * 100).roundToInt() / 100f
        }

        fun onPostScroll(delta: Float, consumed: Float = 0f): Float {
//            var scrolled = value + delta
//            scrolled = if (maxValue > 0)
//                scrolled.coerceIn(0f, maxValue)
//            else
//                scrolled.coerceIn(maxValue, 0f)
//            val currentConsumed = scrolled - value

            var scrolled = value + delta + consumed
            scrolled = if (maxValue > 0)
                scrolled.coerceIn(0f, maxValue)
            else
                scrolled.coerceIn(maxValue, 0f)
            if (value != scrolled) value = scrolled
            return delta // 后续不需要父布局再进行处理
        }

        fun onPreScroll(delta: Float, isFling: Boolean): Float {
            var scrolled = value + delta
            scrolled = if (maxValue > 0)
                scrolled.coerceIn(0f, maxValue)
            else
                scrolled.coerceIn(maxValue, 0f)
            val consumed = scrolled - value
            if (value != scrolled) value = scrolled
            if (isFling && value == 0f) expanded = false
            return consumed
        }

        fun onRelease() {
            if (expanded) {
                if (value == 0f) expanded = false
                return
            }

            if (maxValue > 0) {
                if (value >= threshold) {
                    expanded = true
                } else if (value > 0) {
                    value = 0f
                    expanded = false
                }
            } else {
                if (value <= threshold) {
                    expanded = true
                } else if (value < 0) {
                    value = 0f
                    expanded = false
                }
            }
        }

        companion object {
            private const val THRESHOLD_FACTOR = 0.3
        }
    }
}

private var saver: Saver<CustomNestedScrollState, Any>? = null
private fun getSaver(
    localDensity: Density,
    localConfiguration: Configuration
): Saver<CustomNestedScrollState, Any> {
    if (null == saver) {
        saver = listSaver(
            save = { listOf(it.getTopScrolledPercent()/*, it.getBottomScrolledPercent()*/) },
            restore = {
                CustomNestedScrollState(
                    maxHeightPx = with(localDensity) {
                        localConfiguration.screenHeightDp.dp.toPx()
                    },
                    topScrolledPercent = it[0],
//                    bottomScrolledPercent = it[1]
                )
            }
        )
    }
    return saver as Saver<CustomNestedScrollState, Any>
}

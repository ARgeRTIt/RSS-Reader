package com.leaf.rssreader.ui

import androidx.compose.material3.AlertDialog
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.ui.res.stringResource
import com.leaf.rssreader.R

@Composable
fun RemoveConfirm(
    cancelCallback: () -> Unit,
    confirmCallback: () -> Unit,
) {
    AlertDialog(
        onDismissRequest = cancelCallback,
        title = {
            Text(stringResource(id = R.string.remove_confirm))
        },
        dismissButton = {
            TextButton(onClick = cancelCallback) {
                Text(stringResource(id = R.string.cancel))
            }
        },
        confirmButton = {
            TextButton(onClick = confirmCallback) {
                Text(stringResource(id = R.string.confirm))
            }
        }
    )
}
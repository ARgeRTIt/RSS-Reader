package com.leaf.rssreader.ui

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.*
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.lifecycle.viewmodel.initializer
import androidx.lifecycle.viewmodel.viewModelFactory
import com.leaf.rssreader.DrawerOpenFunc
import com.leaf.rssreader.NavigateCallback
import com.leaf.rssreader.R
import com.leaf.rssreader.Util
import com.leaf.rssreader.pojo.Entry
import com.leaf.rssreader.repository.ChannelRepository
import com.leaf.rssreader.ui.pullrefresh.PullRefreshIndicator
import com.leaf.rssreader.ui.pullrefresh.PullRefreshState
import com.leaf.rssreader.ui.pullrefresh.pullRefresh
import com.leaf.rssreader.ui.pullrefresh.rememberPullRefreshState
import com.leaf.rssreader.viewmodel.ListViewModel
import java.util.*

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun ListCompose(
    navigateCallback: NavigateCallback,
    drawerOpenFunc: DrawerOpenFunc,
    channelId: Int,
    vm: ListViewModel = viewModel(
        key = "list-$channelId",
        factory = viewModelFactory {
            initializer {
                ListViewModel(channelId)
            }
        }
    )
) {
    Scaffold(
        bottomBar = {
            // TODO 横屏时用其他显示方法
            CenterAlignedTopAppBar(
                title = {
                    Text(
                        text = vm.description,
                        maxLines = 1,
                        overflow = TextOverflow.Ellipsis
                    )
                },
                actions = {
                    TextButton(onClick = vm::switchShowState) {
                        Text(text = stringResource(
                            id = if (vm.isShowUnread)
                                R.string.read
                            else
                                R.string.unread
                        ))
                    }
                }
            )

        },
        floatingActionButton = {
            if (vm.isShowUnread && ListViewModel.RefreshState.NORMAL == vm.refreshState) {
                FloatingActionButton(onClick = {
                    ChannelRepository.readAllEntries(channelId)
                    if (null != vm.entryIdWithDivider) vm.entryIdWithDivider = null
                    if (null != drawerOpenFunc) drawerOpenFunc()
                }) {
                    Icon(Icons.Default.Clear, "")
                }
            }
        },
        floatingActionButtonPosition = FabPosition.Center
    ) { paddingValues ->

        val pullRefreshState = rememberPullRefreshState(
            refreshing = ListViewModel.RefreshState.REFRESHING == vm.refreshState,
            onRefresh = vm::refresh
        )
        when (vm.refreshState) {
            ListViewModel.RefreshState.NORMAL -> NormalCompose(
                navigateCallback = navigateCallback,
                channelId = channelId,
                paddingValues = paddingValues,
                pullRefreshState = pullRefreshState,
                vm = vm
            )
            ListViewModel.RefreshState.REFRESHING -> TextCompose(stringResource(R.string.pulling))
            ListViewModel.RefreshState.FAILURE -> TextCompose(
                str = stringResource(R.string.url_cannot_access),
                pullRefreshState = pullRefreshState,
                vm = vm
            )
        }

    }
}

@Composable
private fun TextCompose(
    str: String,
    pullRefreshState: PullRefreshState? = null,
    vm: ListViewModel? = null
) {
    Box(
        modifier = if (null == pullRefreshState)
            Modifier.fillMaxSize()
        else
            Modifier
                .pullRefresh(pullRefreshState)
                .fillMaxSize()
                .verticalScroll(rememberScrollState()),
        contentAlignment = Alignment.Center
    ) {
        Text(str)

        if (null != pullRefreshState) {
            PullRefreshIndicator(
                refreshing = ListViewModel.RefreshState.REFRESHING == vm!!.refreshState,
                state = pullRefreshState,
                modifier = Modifier.align(Alignment.TopCenter)
            )
        }
    }
}

@Composable
private fun NormalCompose(
    navigateCallback: (String?) -> Unit,
    channelId: Int,
    paddingValues: PaddingValues,
    pullRefreshState: PullRefreshState,
    vm: ListViewModel
) {
    val entries by Util.generateLifecycleAwareFlow(
        flow = if (vm.isShowUnread)
            ChannelRepository.getUnreadEntriesByChannelId(channelId)
        else
            ChannelRepository.getReadEntriesByChannelId(channelId)
    ).collectAsState(initial = emptyList())

    val isEntriesEmpty = entries.isEmpty()
    Box(
        modifier = if (vm.isShowUnread && isEntriesEmpty)
            Modifier.pullRefresh(pullRefreshState)
        else
            Modifier
    ) {
        if (isEntriesEmpty) {
            if (vm.isShowUnread) {
                // 为了使 nestedScroll 生效，以及 LazyColumn 正常默认显示第二个 item
                Box(
                    modifier = Modifier
                        .fillMaxSize()
                        .verticalScroll(rememberScrollState())
                )
            }
        } else {
            val entriesIndices = entries.indices
            val containerColor = if (vm.isShowUnread)
                MaterialTheme.colorScheme.primaryContainer
            else
                MaterialTheme.colorScheme.tertiaryContainer

            CustomLazyColumn(
                modifier = Modifier.padding(paddingValues),
                contentPadding = PaddingValues(8.dp, 4.dp, 8.dp, 4.dp),
                verticalArrangement = Arrangement.spacedBy(8.dp)
            ) {
                val currentLocalDateTime = Util.getCurrentLocalDateTime()

                for (i in entries.indices) {
                    val entry = entries[i]
                    val id = entry.getUniqueId()
                    if (id == vm.entryIdWithDivider) {
                        item(
                            contentType = "item-divider"
                        ) {
                            Box(
                                contentAlignment = Alignment.Center
                            ) {
                                Divider()
                                Text(
                                    text = stringResource(id = R.string.last_read_position),
                                    style = MaterialTheme.typography.labelSmall,
                                    modifier = Modifier
                                        .background(MaterialTheme.colorScheme.background)
                                        .padding(8.dp, 0.dp)
                                )
                            }
                        }
                    }

                    item(
                        key = id,
                        contentType = Entry
                    ) {
                        EntryCard(
                            entry = entry,
                            currentLocalDateTime = currentLocalDateTime,
                            colors = CardDefaults.cardColors(
                                containerColor = containerColor
                            ),
                            modifier = Modifier.fillMaxWidth()
                        ) {
                            navigateCallback(
                                NavRoute.DETAIL +
                                        "/${Base64.getUrlEncoder().encodeToString(id.toByteArray())}"
                            )

                            if (vm.isShowUnread) {
                                if (i > entriesIndices.first && i < entriesIndices.last) {
                                    vm.entryIdWithDivider = entries[i + 1].getUniqueId()
                                } else if (null != vm.entryIdWithDivider) {
                                    vm.entryIdWithDivider = null
                                }
                                ChannelRepository.readEntry(channelId, id)
                            }

                        }
                    }
                }
            }
        }

        PullRefreshIndicator(
            refreshing = ListViewModel.RefreshState.REFRESHING == vm.refreshState,
            state = pullRefreshState,
            modifier = Modifier.align(Alignment.TopCenter)
        )
    }
}

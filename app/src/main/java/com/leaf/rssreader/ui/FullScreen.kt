package com.leaf.rssreader.ui

import androidx.activity.compose.BackHandler
import androidx.compose.foundation.Canvas
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.gestures.detectTransformGestures
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.graphicsLayer
import androidx.compose.ui.input.pointer.pointerInput
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.unit.dp
import coil.compose.AsyncImage
import com.leaf.rssreader.R
import com.leaf.rssreader.pojo.FullScreenDTO
import com.leaf.rssreader.pojo.FullScreenDTOType

@Composable
fun FullScreen(
    fullScreenDTO: FullScreenDTO,
    dismissCallback: () -> Unit
) = when (fullScreenDTO.type) {
    FullScreenDTOType.IMAGE -> {
        FullScreenImage(
            dismissCallback = dismissCallback,
            imageUrl = fullScreenDTO.data as String
        )
    }
    FullScreenDTOType.PICK_READ_ENTRIES_CACHED_DAYS -> {
        IntPicker(
            dismissCallback = dismissCallback,
            data = fullScreenDTO.data as Int,
            confirmCallback = fullScreenDTO.confirmCallback!!,
            dataLower = 1,
            dataUpper = Byte.MAX_VALUE.toInt(),
            title = stringResource(id = R.string.read_entries_cached_days)
        )
    }
    FullScreenDTOType.REMOVE_CONFIRM -> {
        AlertDialog(
            onDismissRequest = dismissCallback,
            onConfirmRequest = { fullScreenDTO.confirmCallback!!(null) },
            title = stringResource(id = R.string.remove_confirm)
        )
    }
    FullScreenDTOType.PICK_FONT_SCALE_EXTRA -> {
        IntPicker(
            dismissCallback = dismissCallback,
            data = fullScreenDTO.data as Int,
            confirmCallback = fullScreenDTO.confirmCallback!!,
            dataLower = 0,
            dataUpper = 20,
            title = stringResource(id = R.string.font_scale_extra_multiply_by_10)
        )
    }
}

@Composable
private fun FullScreenImage(dismissCallback: () -> Unit, imageUrl: String) {
    BackHandler(onBack = dismissCallback)

    var scale by remember { mutableStateOf(1f) }
    var offset by remember { mutableStateOf(Offset.Zero) }

    AsyncImage(
        model = imageUrl,
        contentDescription = "",
        modifier = Modifier
            .fillMaxSize()
            .background(MaterialTheme.colorScheme.background)
            .pointerInput(Unit) {
                // TODO 缩放时，按两指中间为中点
                // TODO 限制平移范围
                detectTransformGestures(true) { _, pan, zoom, _ ->
                    if (zoom == 1f) {
                        offset += pan
                        return@detectTransformGestures
                    }

                    scale = (scale * zoom).coerceIn(0.5f, 3f)
                }
            }
            .graphicsLayer(
                scaleX = scale,
                scaleY = scale,
                translationX = offset.x,
                translationY = offset.y
            )
    )
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
private fun IntPicker(
    dismissCallback: () -> Unit,
    data: Int,
    confirmCallback: (Any?) -> Unit,
    dataLower: Int,
    dataUpper: Int,
    title: String?
) {
    var dataStr by remember {
        mutableStateOf(data.toString())
    }

    val state by remember {
        derivedStateOf {
            val tempData = dataStr.toIntOrNull() ?: return@derivedStateOf -1
            if (tempData < dataLower) return@derivedStateOf -1
            if (tempData > dataUpper) return@derivedStateOf -2
            0
        }
    }

    AlertDialog(
        onDismissRequest = dismissCallback,
        onConfirmRequest = {
            confirmCallback(dataStr.toIntOrNull())
        },
        confirmButtonEnabled = 0 == state,
        title = title
    ) {
        TextField(
            modifier = Modifier.fillMaxWidth(),
            value = dataStr,
            onValueChange = { dataStr = it },
            keyboardOptions = KeyboardOptions.Default.copy(
                keyboardType = KeyboardType.Number
            ),
            isError = 0 != state,
            supportingText = {
                Text(
                    text = when (state) {
                        -1 -> {
                            "${stringResource(
                                id = R.string.must_greater_than_or_equal_to
                            )} $dataLower"
                        }
                        -2 -> {
                            "${stringResource(id = R.string.must_less_than_or_equal_to)} $dataUpper"
                        }
                        else -> ""
                    }
                )
            },
            trailingIcon = if (0 != state) {{
                Icon(painterResource(id = R.drawable.ic_baseline_error_24), "")
            }} else null
        )
    }
}

@Composable
private fun AlertDialog(
    onDismissRequest: () -> Unit,
    onConfirmRequest: () -> Unit,
    confirmButtonEnabled: Boolean = true,
    title: String? = null,
    content: (@Composable () -> Unit)? = null,
) {
    BackHandler(onBack = onDismissRequest)

    Box(
        modifier = Modifier.fillMaxSize(),
        contentAlignment = Alignment.Center
    ) {
        Scrim(onDismissRequest)

        Surface(
            shape = AlertDialogDefaults.shape,
            color = AlertDialogDefaults.containerColor,
            modifier = Modifier.padding(horizontal = 45.dp),
            tonalElevation = AlertDialogDefaults.TonalElevation
        ) {
            Column(
                modifier = Modifier
                    .sizeIn(minWidth = 280.dp, maxWidth = 560.dp)
                    .padding(24.dp)
            ) {
                title?.let {
                    Box(modifier = Modifier.padding(bottom = 16.dp)) {
                        Text(
                            text = title,
                            style = MaterialTheme.typography.headlineSmall,
                            color = MaterialTheme.colorScheme.onSurface
                        )
                    }
                }
                content?.let {
                    Box(modifier = Modifier.padding(bottom = 12.dp)) {
                        content()
                    }
                }

                FlowRow(
                    modifier = Modifier.fillMaxWidth(),
                    horizontalArrangement = Arrangement.End
                ) {
                    TextButton(onClick = onDismissRequest) {
                        Text(stringResource(id = R.string.cancel))
                    }
                    TextButton(
                        enabled = confirmButtonEnabled,
                        onClick = {
                            onConfirmRequest()
                            onDismissRequest()
                        }
                    ) {
                        Text(stringResource(id = R.string.confirm))
                    }
                }
            }
        }
    }
}

// 参考 NavigationDrawer
@Composable
private fun Scrim(
    onClose: () -> Unit,
) {
    val color = MaterialTheme.colorScheme.scrim
    Canvas(
        Modifier
            .fillMaxSize()
            .clickable(onClick = onClose)
    ) {
        drawRect(color = color, alpha = 0.6f)
    }
}

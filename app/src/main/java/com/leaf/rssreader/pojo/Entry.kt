package com.leaf.rssreader.pojo

import org.json.JSONObject

data class Entry(
    val title: String,
    val description: String,
    val pubDate: Long,
    val link: String,
    val primaryId: String? = null,
    val readDate: Long? = null
) {

    fun getUniqueId(): String {
        return primaryId ?: link
    }

    fun convertToJson(): JSONObject {
        val obj = JSONObject()
        obj.put(JsonKey.TITLE, title)
        obj.put(JsonKey.DESCRIPTION, description)
        obj.put(JsonKey.PUB_DATE, pubDate)
        obj.put(JsonKey.LINK, link)
        if (null != primaryId) obj.put(JsonKey.PRIMARY_ID, primaryId)
        if (null != readDate) obj.put(JsonKey.READ_DATE, readDate)
        return obj
    }

    companion object {
        fun convertFromJson(obj: JSONObject): Entry {
            return Entry(
                title = obj.getString(JsonKey.TITLE),
                description = obj.getString(JsonKey.DESCRIPTION),
                pubDate = obj.getLong(JsonKey.PUB_DATE),
                link = obj.getString(JsonKey.LINK),
                primaryId = if (obj.has(JsonKey.PRIMARY_ID))
                    obj.getString(JsonKey.PRIMARY_ID)
                else null,
                readDate = if (obj.has(JsonKey.READ_DATE)) obj.getLong(JsonKey.READ_DATE) else null
            )
        }
    }

    // -------- private fun BEGIN --------

    private object JsonKey {
        const val TITLE = "title"
        const val DESCRIPTION = "desc"
        const val PUB_DATE = "pubDate"
        const val LINK = "link"
        const val PRIMARY_ID = "pid"
        const val READ_DATE = "readDate"
    }

    // -------- private fun END --------
}
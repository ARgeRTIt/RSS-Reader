package com.leaf.rssreader.pojo

import org.json.JSONObject

data class Channel(
    val id: Int,
    val url: String,
    val description: String
) {
    fun convertToJson(): JSONObject {
        val obj = JSONObject()
        obj.put(JsonKey.ID, id)
        obj.put(JsonKey.URL, url)
        obj.put(JsonKey.DESCRIPTION, description)
        return obj
    }

    companion object {
        fun convertFromJson(obj: JSONObject): Channel {
            return Channel(
                id = obj.getInt(JsonKey.ID),
                url = obj.getString(JsonKey.URL),
                description = obj.getString(JsonKey.DESCRIPTION)
            )
        }
    }

    // -------- private fun BEGIN --------

    private object JsonKey {
        const val ID = "id"
        const val URL = "url"
        const val DESCRIPTION = "desc"
    }

    // -------- private fun END --------
}
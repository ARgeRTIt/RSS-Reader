package com.leaf.rssreader.pojo

import androidx.compose.ui.text.AnnotatedString

data class Detail(
    val contentList: List<Pair<String, AnnotatedString>>,
)

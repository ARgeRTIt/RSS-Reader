package com.leaf.rssreader.pojo

import com.leaf.rssreader.repository.SettingRepository
import kotlinx.coroutines.flow.MutableStateFlow
import org.json.JSONArray
import org.json.JSONObject
import java.time.ZonedDateTime

data class EntryCache(
    var lastRefreshTimestamp: Long,
    val unreadEntries: MutableStateFlow<List<Entry>>,
    private val readEntries: MutableStateFlow<List<Entry>>,
) {
    fun getReadEntries(): MutableStateFlow<List<Entry>> {
        if (readEntries.value.isEmpty()) return readEntries

        val longestTimestamp = getLongestTimestamp()
        var readEntriesMutableList: MutableList<Entry>? = null
        for (i in readEntries.value.indices) {
            val entry = readEntries.value[i]
            if (entry.readDate!! < longestTimestamp) {
                if (null == readEntriesMutableList) {
                    readEntriesMutableList = MutableList(i + 1) {
                        readEntries.value[it]
                    }
                }
                continue
            } else readEntriesMutableList?.add(entry)
        }

        if (null != readEntriesMutableList) readEntries.value = readEntriesMutableList
        return readEntries
    }

    fun setReadEntries(entries: List<Entry>) {
        readEntries.value = entries
    }

    fun convertToJson(): JSONObject {
        val longestTimestamp = getLongestTimestamp()
        val obj = JSONObject()
        obj.put(JsonKey.LAST_REFRESH_TIMESTAMP, lastRefreshTimestamp)
        val unreadArr = JSONArray()
        for (entry in unreadEntries.value) {
            unreadArr.put(entry.convertToJson())
        }
        obj.put(JsonKey.UNREAD_ENTRIES, unreadArr)
        val readArr = JSONArray()
        for (entry in readEntries.value) {
            if (entry.readDate!! < longestTimestamp) continue
            readArr.put(entry.convertToJson())
        }
        obj.put(JsonKey.READ_ENTRIES, readArr)
        return obj
    }

    companion object {
        fun convertFromJson(obj: JSONObject): EntryCache {
            val longestTimestamp = getLongestTimestamp()
            val unreadArr = obj.getJSONArray(JsonKey.UNREAD_ENTRIES)
            val unreadEntries = mutableListOf<Entry>()
            for (i in 0 until unreadArr.length()) {
                unreadEntries.add(Entry.convertFromJson(unreadArr.getJSONObject(i)))
            }
            val readArr = obj.getJSONArray(JsonKey.READ_ENTRIES)
            val readEntries = mutableListOf<Entry>()
            for (i in 0 until readArr.length()) {
                val entry = Entry.convertFromJson(readArr.getJSONObject(i))
                checkNotNull(entry.readDate)
                if (entry.readDate < longestTimestamp) break
                readEntries.add(entry)
            }
            return EntryCache(
                lastRefreshTimestamp = obj.getLong(JsonKey.LAST_REFRESH_TIMESTAMP),
                unreadEntries = MutableStateFlow(unreadEntries),
                readEntries = MutableStateFlow(readEntries)
            )
        }

        private fun getLongestTimestamp(): Long {
            return ZonedDateTime.now()
                .withHour(0)
                .withMinute(0)
                .withSecond(0)
                .withNano(0)
                .plusDays(-SettingRepository.settingStateFlow.value.readEntriesCachedDays.toLong())
                .toEpochSecond()
        }
    }

    // -------- private fun BEGIN --------

    private object JsonKey {
        const val LAST_REFRESH_TIMESTAMP = "lrt"
        const val UNREAD_ENTRIES = "unread"
        const val READ_ENTRIES = "read"
    }

    // -------- private fun END --------
}
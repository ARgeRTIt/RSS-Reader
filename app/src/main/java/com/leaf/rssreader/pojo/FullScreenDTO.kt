package com.leaf.rssreader.pojo

data class FullScreenDTO(
    val type: FullScreenDTOType,
    val data: Any? = null,
    val confirmCallback: ((Any?) -> Unit)? = null
)

enum class FullScreenDTOType {
    IMAGE,
    PICK_READ_ENTRIES_CACHED_DAYS,
    REMOVE_CONFIRM,
    PICK_FONT_SCALE_EXTRA,
}

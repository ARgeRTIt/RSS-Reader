package com.leaf.rssreader.pojo

import org.json.JSONObject

data class Setting(
    val mobileNetworkDescriptionImageShow: Boolean = false,
    val mobileNetworkFullContentImageShow: Boolean = false,
    val readEntriesCachedDays: Int = 4,
    val fontScaleExtraMultiplyBy10: Int = 5
) {
    fun getFontScaleExtra(): Float {
        return fontScaleExtraMultiplyBy10 / 10f
    }

    fun convertToJsonString(): String {
        val obj = JSONObject()
        obj.put(JsonKey.MOBILE_NETWORK_DESCRIPTION_IMAGE_SHOW, mobileNetworkDescriptionImageShow)
        obj.put(JsonKey.MOBILE_NETWORK_FULL_CONTENT_IMAGE_SHOW, mobileNetworkFullContentImageShow)
        obj.put(JsonKey.READ_ENTRIES_CACHED_DAYS, readEntriesCachedDays)
        obj.put(JsonKey.FONT_SCALE_EXTRA_MULTIPLY_BY_10, fontScaleExtraMultiplyBy10)
        return obj.toString(4)
    }

    companion object {
        fun convertFromJsonString(str: String): Setting {
            val obj = JSONObject(str)
            return Setting(
                mobileNetworkDescriptionImageShow = obj.getBoolean(
                    JsonKey.MOBILE_NETWORK_DESCRIPTION_IMAGE_SHOW
                ),
                mobileNetworkFullContentImageShow = obj.getBoolean(
                    JsonKey.MOBILE_NETWORK_FULL_CONTENT_IMAGE_SHOW
                ),
                readEntriesCachedDays = obj.getInt(
                    JsonKey.READ_ENTRIES_CACHED_DAYS
                ),
                fontScaleExtraMultiplyBy10 = obj.getInt(
                    JsonKey.FONT_SCALE_EXTRA_MULTIPLY_BY_10
                )
            )
        }
    }

    // -------- private fun BEGIN --------

    private object JsonKey {
        const val MOBILE_NETWORK_DESCRIPTION_IMAGE_SHOW = "mndis"
        const val MOBILE_NETWORK_FULL_CONTENT_IMAGE_SHOW = "mnfcis"
        const val READ_ENTRIES_CACHED_DAYS = "recd"
        const val FONT_SCALE_EXTRA_MULTIPLY_BY_10 = "fsemb10"
    }

    // -------- private fun END --------

}
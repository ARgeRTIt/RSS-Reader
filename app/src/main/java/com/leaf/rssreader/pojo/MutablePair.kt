package com.leaf.rssreader.pojo

data class MutablePair<A, B>(
    var first: A,
    val second: B
)

package com.leaf.rssreader.pojo

import org.json.JSONArray
import org.json.JSONObject

data class Tag(
    val name: String,
    val channelIds: MutableList<Int> // 有序列表
) {

    fun convertToJson(): JSONObject {
        val obj = JSONObject()
        obj.put(JsonKey.NAME, name)
        obj.put(JsonKey.CHANNEL_IDS, JSONArray(channelIds))
        return obj
    }

    companion object {
        fun convertFromJson(obj: JSONObject): Tag {
            val arr = obj.getJSONArray(JsonKey.CHANNEL_IDS)

            val channelIds = MutableList(
                size = arr.length(),
                init = { arr.getInt(it) }
            ).distinct().toMutableList()
            channelIds.sort()

            return Tag(
                name = obj.getString(JsonKey.NAME),
                channelIds = channelIds
            )
        }
    }

    // -------- private fun BEGIN --------

    private object JsonKey {
        const val NAME = "id"
        const val CHANNEL_IDS = "cids"
    }

    // -------- private fun END --------

}

package com.leaf.rssreader

import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.withStyle
import com.leaf.rssreader.pojo.Detail
import com.leaf.rssreader.pojo.MutablePair
import org.apache.commons.text.StringEscapeUtils
import org.jsoup.nodes.Element
import org.jsoup.nodes.Node
import org.jsoup.nodes.TextNode

object HtmlParser {

    enum class ValidHtmlTagEnum(val str: String) {
        P("p"),
        IMG("img"),
        LI("li"),
        H("h")
    }

    fun extractValidElements(nodes: List<Node>): Detail {
        val originList = extractValidText(nodes)
        tryRemoveLastEmptyLine(originList)

        val contentMutableList = mutableListOf<Pair<String, AnnotatedString>>()
        //var imageContainedFlag = false

        for ((tagName, builder) in originList) {
            contentMutableList.add(Pair(tagName, builder.toAnnotatedString()))
            //if (!imageContainedFlag && ValidHtmlTagEnum.IMG.str == tagName) {
            //    imageContainedFlag = true
            //}
        }

        return Detail(
            contentMutableList,
        )
    }

    // -------- private function BEGIN --------

    private fun tryAddNewLine(
        contentMutableList: MutableList<MutablePair<String, AnnotatedString.Builder>>,
        tagName: String = ValidHtmlTagEnum.P.str
    ) {
        if (contentMutableList.size > 0
            && contentMutableList.last().second.length == 0) {
            contentMutableList.last().first = tagName
            return
        }
        contentMutableList.add(
            MutablePair(
                tagName,
                AnnotatedString.Builder()
            )
        )
    }

    private fun tryRemoveLastEmptyLine(
        contentMutableList: MutableList<MutablePair<String, AnnotatedString.Builder>>
    ) {
        if (contentMutableList.size == 0) return
        if (contentMutableList.last().second.length > 0) return
        contentMutableList.removeLast()
    }

    private fun extractValidText(
        nodes: List<Node>,
        contentMutableList: MutableList<MutablePair<String, AnnotatedString.Builder>> =
            mutableListOf(
                MutablePair(
                    ValidHtmlTagEnum.P.str,
                    AnnotatedString.Builder()
                )
            ),
        spanStyle: SpanStyle? = null
    ): MutableList<MutablePair<String, AnnotatedString.Builder>> {
        var lastInlineFlag = false // 上一个元素是否有 inline 属性
        for (node in nodes) {
            when (node) {
                is TextNode -> {
                    val text = StringEscapeUtils.unescapeHtml4(Util.processText(node.text()))
                    if (text.isBlank()) continue
                    contentMutableList.last().second.apply {
                        if (null == spanStyle) {
                            append(text)
                        } else {
                            withStyle(spanStyle) {
                                append(text)
                            }
                        }
                    }
                }
                is Element -> {
                    var inlineFlag = false
                    when (val tagName = node.tagName()) {
                        ValidHtmlTagEnum.P.str -> {
                            inlineFlag = node.attr("style")
                                .startsWith("display: inline;")
                            if ((!lastInlineFlag) && !inlineFlag) {
                                tryAddNewLine(contentMutableList, tagName)
                            }
                            extractValidText(node.childNodes(), contentMutableList, spanStyle)
                            if (!inlineFlag) tryAddNewLine(contentMutableList)
                        }
                        "a" -> {
                            extractValidText(
                                node.childNodes(),
                                contentMutableList,
                                spanStyle
                            )
                            inlineFlag = lastInlineFlag
                        }
                        "strong" -> {
                            extractValidText(
                                node.childNodes(),
                                contentMutableList,
                                spanStyle ?: SpanStyle(
                                    fontWeight = FontWeight.Bold
                                )
                            )
                        }
                        "br" -> {
                            tryAddNewLine(contentMutableList)
                        }
                        "div" -> {
                            tryAddNewLine(contentMutableList)
                            extractValidText(node.childNodes(), contentMutableList)
                        }
                        ValidHtmlTagEnum.IMG.str -> {
                            tryRemoveLastEmptyLine(contentMutableList)
                            contentMutableList.add(
                                MutablePair(
                                    ValidHtmlTagEnum.IMG.str,
                                    AnnotatedString.Builder().apply {
                                        append(Util.processUrl(node.attr("src")))
                                    }
                                )
                            )
                            tryAddNewLine(contentMutableList)
                        }
                        ValidHtmlTagEnum.LI.str -> {
                            tryAddNewLine(contentMutableList, tagName)
                            //extractValidText(node.childNodes(), contentMutableList)
                            contentMutableList.last().second.append(
                                StringEscapeUtils.unescapeHtml4(Util.processText(node.text()))
                            )
                            tryAddNewLine(contentMutableList)
                        }
                        else -> {
                            if (tagName.startsWith(ValidHtmlTagEnum.H.str)) {
                                tryAddNewLine(contentMutableList, ValidHtmlTagEnum.H.str)
                                extractValidText(node.childNodes(), contentMutableList)
                                tryAddNewLine(contentMutableList)
                            } else {
                                extractValidText(node.childNodes(), contentMutableList)
                            }
                        }
                    }
                    lastInlineFlag = inlineFlag
                }
                else -> {}
            }
        }
        return contentMutableList
    }

    // -------- private function END --------

}
package com.leaf.rssreader

import com.leaf.rssreader.repository.ChannelRepository
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.MutableSharedFlow
import okhttp3.OkHttpClient
import okhttp3.Request
import java.io.InputStream
import java.util.concurrent.TimeUnit

object DownloadService {

    private const val DOWNLOAD_INTERVAL = 120 // seconds
    private val downloadingUrlMap: MutableMap<String, Job> = HashMap()
    val okHttpClient = OkHttpClient.Builder()
        .connectTimeout(5, TimeUnit.SECONDS)
        //.retryOnConnectionFailure(false)
        .build()
    val progress = MutableSharedFlow<Pair<String, State>>()

    fun downloadFeed(
        channelId: Int,
        url: String,
        currentTimestamp: Long = Util.getCurrentTimestamp()
    ) {
        val lastRefreshTimestamp =
            ChannelRepository.getEntryCacheLastRefreshTimestamp(channelId)
        if (currentTimestamp - lastRefreshTimestamp <= DOWNLOAD_INTERVAL) {
            CoroutineScope(Dispatchers.IO).launch {
                // Modifier.pullRefresh 由 mutableStateOf 通知 refreshing 更新，
                // 连续赋值会导致只有最新的值能通知到
                delay(500)
                progress.emit(Pair(url, State.SUCCESS))
            }
            return
        }

        download(
            url = url
        ) { inputStream ->
            val tempEntries = FeedParser.parser(inputStream, lastRefreshTimestamp)
            ChannelRepository.saveNewEntries(channelId, tempEntries, currentTimestamp)
        }
    }

    fun download(
        url: String,
        inputStreamHandler: suspend (InputStream) -> Unit // 抛出异常则视为下载失败
    ) {
        if (downloadingUrlMap.containsKey(url)) return

        val job = CoroutineScope(Dispatchers.IO).launch {
            progress.emit(Pair(url, State.DOWNLOADING))

            try {
                // TODO execute 能不能使用 协程
                okHttpClient.newCall(
                    Request.Builder()
                        .url(url)
                        .build()
                ).execute().use { response ->
                    downloadingUrlMap.remove(url)

                    if (!response.isSuccessful) {
                        //println("------- download response failure")
                        progress.emit(Pair(url, State.FAILURE))
                        return@launch
                    }
                    if (null == response.body) {
                        //println("------- download empty body")
                        progress.emit(Pair(url, State.FAILURE))
                        return@launch
                    }
                    response.body!!.byteStream().use {
                        try {
                            inputStreamHandler(it)
                        } catch (e: Exception) {
                            //println("------- download input stream handler")
                            Log.e(e.stackTraceToString())
                            progress.emit(Pair(url, State.FAILURE))
                            return@launch
                        }
                        progress.emit(Pair(url, State.SUCCESS))
                    }
                }
            } catch (e: Exception) {
                //println("------- download cannot access")
                Log.e(e.stackTraceToString())
                downloadingUrlMap.remove(url)
                progress.emit(Pair(url, State.FAILURE))
            }
        }
        downloadingUrlMap[url] = job
    }

    enum class State {
        DOWNLOADING,
        SUCCESS,
        FAILURE
    }

}